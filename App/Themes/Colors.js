const colors = {
  panther: '#161616',
  coal: '#2d2d2d',
  charcoal: '#595959',
  steel: '#CCCCCC',
  frost: '#D8D8D8',
  silver: '#F7F7F7',
  transparent: 'rgba(0,0,0,0)',
  background: 'white',
  facebook: '#3b5998',
  google: '#dd4b39',
  whatsapp: '#25D366',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',
  badge: '#ff0000',

  amber: '#FFC107',
  red: '#FF5722',
  green: '#4CAF50',
  blue: '#2196F3',

  onewaleet: '#FFC107',
  telecash: '#FF5722',
  ecocash: '#2196F3'
}

export default colors
