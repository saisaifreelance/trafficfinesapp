import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  title: {
    ...Fonts.style.h4,
    color: Colors.snow,
  },
  backgroundOverlayBlue: {
    ...ApplicationStyles.screen.backgroundOverlay,
    backgroundColor: Colors.blue,
    opacity: 0.5
  },
  backgroundOverlayRed: {
    ...ApplicationStyles.screen.backgroundOverlay,
    backgroundColor: Colors.red,
    opacity: 0.5
  }
})
