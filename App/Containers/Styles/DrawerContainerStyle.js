import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background
  },
  optionsContainer: {
    backgroundColor: Colors.snow
  },
  optionContainer: {
    flexDirection: 'row',
    padding: Metrics.smallMargin,
    alignItems: 'center'
  },
  optionIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: Metrics.smallMargin
  },
  option: {
    ...Fonts.style.h6,
    flex: 1,
    color: Colors.charcoal
  },
  bottom: {}
})
