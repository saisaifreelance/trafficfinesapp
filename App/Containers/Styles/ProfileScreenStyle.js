import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.logo,
  logoContainer: {
    ...ApplicationStyles.logo.logoContainer,
    flex: 1
  },
  logoCenter: {
    ...ApplicationStyles.logo.logoContainer
  },
  container: {
    ...ApplicationStyles.screen.container,
    backgroundColor: Colors.snow,
    paddingTop: Metrics.toolbarHeight,
  }
})
