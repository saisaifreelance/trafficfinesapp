import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // right: 0,
    backgroundColor: Colors.charcoal,
  },
  contentContainer: {
    flexDirection: 'row',
    padding: Metrics.smallMargin,
    paddingBottom: 0,
  },
  iconContainer: {
    width: Metrics.icons.large,
    height: Metrics.icons.large,
    borderRadius: Metrics.icons.large / 2,
  },
  icon: {
    width: Metrics.icons.large,
    height: Metrics.icons.large,
    borderRadius: Metrics.icons.large / 2,
    backgroundColor: Colors.snow
  },
  textContainer: {
    marginLeft: Metrics.smallMargin,
    flex: 1,
  },
  title: {
    color: Colors.snow,
    fontFamily: Fonts.type.bold,
    fontWeight: 'bold',
    fontSize: Fonts.size.small,
  },
  description: {
    color: Colors.snow,
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.small,
  },
  pullDownContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  pullDown: {
    backgroundColor: Colors.steel,
    height: Metrics.smallMargin,
    width: Metrics.searchBarHeight,
    borderRadius: Metrics.smallMargin / 2,
    marginVertical: Metrics.smallMargin
  }
})
