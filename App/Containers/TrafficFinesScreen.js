import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { createStructuredSelector } from 'reselect'
import { equals } from 'ramda'
import { FlatList, LayoutAnimation, View, Text, BackHandler, Platform } from 'react-native'
import PropTypes from 'prop-types'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import Actions, { makeSelectTrafficFines } from '../Redux/TrafficFinesRedux'

import NavBar from '../Components/NavBar'
import Header from '../Components/Header'
import { Images, Colors, Metrics } from '../Themes'
import styles from './Styles/TrafficFinesScreenStyle'
import ListItemTrafficFineCategory from '../Components/ListItemTrafficFineCategory'
import ListItemTrafficFine from '../Components/ListItemTrafficFine'
// import ProgressModal from '../Components/ProgressModal'

const {Types, Properties} = LayoutAnimation

const TRANSITION_DURATION = 300
const PRESET = LayoutAnimation.create(TRANSITION_DURATION, Types.easeInEaseOut, Properties.opacity)

class TrafficFinesScreen extends Component {
  // // Prop type warnings
  static propTypes = {
    trafficFines: PropTypes.shape({}).isRequired,
  }

  constructor (props) {
    super(props)
    this.backAction = this.backAction.bind(this)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.handleItemPress = this.handleItemPress.bind(this)
    this.handleItemLongPress = this.handleItemLongPress.bind(this)
    this.renderItem = this.renderItem.bind(this)
    this.renderHeader = this.renderHeader.bind(this)

    const state = {
      selectedCardIndex: {
        cat: null,
        item: null,
      },
      hoveredCardIndex: {
        cat: null,
        item: null,
      },
      animating: false,
    }
    const data = this.getItems(props.trafficFines, state)
    this.state = {
      ...state,
      data
    }
  }

  trafficFineAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'TrafficFineScreen',
    params: {}
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
    const data = this.getItems(this.props.trafficFines, this.state)
    this.setState({data})
  }

  componentDidUpdate (prevProps, prevState) {
    if (
      !equals(this.state.selectedCardIndex, prevState.selectedCardIndex) ||
      !equals(this.state.hoveredCardIndex, prevState.hoveredCardIndex) ||
      this.state.animating !== prevState.animating
    ) {
      const data = this.getItems(this.props.trafficFines, this.state)
      this.setState({data})
    }

    if (!equals(this.props.trafficFines, prevProps.trafficFines)) {
      const data = this.getItems(this.props.trafficFines, this.state)
      this.setState({data})
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  getItems (trafficFinesDomain, state) {
    const { trafficFines, trafficFinesCategories, isSearching, query, searchResults } = trafficFinesDomain
    const { selectedCardIndex, hoveredCardIndex, animating } = state
    let data = []
    let title = 'Traffic fines categories'
    if (!trafficFines || !trafficFinesCategories) {
      return data
    }

    if (isSearching) {
      title = isSearching
    }

    if (query && Array.isArray(searchResults)) {
      title = `Found ${searchResults.length} results for your query "${query}"`
      data = searchResults.map((fine) => ({key: fine.id, selected: false, hovered: false, type: 'fine'}))
      return data
    }

    const categoryKeys = Object.keys(trafficFinesCategories)
    // if (!animating && (selectedCardIndex.cat || selectedCardIndex.cat === 0)) {
    if (selectedCardIndex.cat) {
      const categoryKey = selectedCardIndex.cat
      const category = trafficFinesCategories[categoryKey]
      data = Object.keys(category.traffic_fines).map((key) => ({key, selected: false, hovered: false, type: 'fine'}))
      return data
    }

    data = categoryKeys.map((key, index) => ({key, type: 'cat', selected: selectedCardIndex.cat === index, hovered: hoveredCardIndex.cat === index}))
    return data

    // return [{key: 1, selected: 1, hovered: 1}, {key: 2, selected: 1, hovered: 1}, {key: 3, selected: 1, hovered: 1}]
  }

  backAction () {
    if (this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0) {
      const state = {
        selectedCardIndex: {
          cat: null,
          item: null,
        },
        hoveredCardIndex: {
          cat: null,
          item: null,
        },
        animating: false,
      }
      const data = this.getItems(this.props.trafficFines, state)
      this.setState({...state, data})
      return true
    }
    this.props.navigation.goBack()
    return true
  }

  keyExtractor (item, index) {
    return item.key
  }

  handleItemPress (cardId, id) {
    this.trafficFineAction.params.id = id
    this.props.navigation.dispatch(this.trafficFineAction)
  }

  handleItemLongPress (cardId, id) {
    this.trafficFineAction.params.id = id
    this.props.navigation.dispatch(this.trafficFineAction)
  }

  handlePress (cardId) {
    if (this.state.selectedCardIndex.cat === cardId && this.state.hoveredCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(PRESET, () => { this.setState({animating: false}) })
    // this.list.scrollTo({y: 0, x: 0, animated: true})
    this.setState({
      selectedCardIndex: {
        cat: cardId,
        item: null
      },
      hoveredCardIndex: {
        cat: null,
        item: null
      },
      animating: true
    })
  }

  handleLongPress (cardId) {
    if (this.state.hoveredCardIndex.cat === cardId && this.state.selectedCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => { this.setState({animating: false})})
    this.setState({
      selectedCardIndex: {
        cat: null,
        item: null
      },
      hoveredCardIndex: {
        cat: cardId,
        item: null
      },
      animating: true
    })
  }

  renderHeader () {
    const {isSearching, query, searchResults} = this.props.trafficFines
    let title = 'Traffic fines categories'
    let cat = null
    if (isSearching) {
      title = isSearching
    } else if (query && Array.isArray(searchResults)) {
      title = `Found ${searchResults.length} results for your query "${query}"`
    } else if (this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0) {
      title = 'Traffic fines'
    }
    if (this.state.selectedCardIndex.cat) {
      const category = this.props.trafficFines.trafficFinesCategories[this.state.selectedCardIndex.cat]
      cat = (
        <ListItemTrafficFineCategory
          category={category}
          cardIndex={0}
          selected={0}
          hovered={0}
          onPress={() => {}}
          onLongPress={() => {}}
        />
      )
    }
    return (
      <View>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{title}</Text>
        </View>
        {cat}
      </View>
    )
  }

  renderItem ({item: {key, selected, hovered}, index, separators}) {
    const category = this.props.trafficFines.trafficFinesCategories[key]
    if (category) {
      return (
        <ListItemTrafficFineCategory
          category={category}
          cardIndex={index}
          selected={this.state.selectedCardIndex.cat}
          hovered={this.state.hoveredCardIndex.cat}
          onPress={this.handlePress}
          onLongPress={this.handleLongPress}
        />
      )
    }

    const fine = this.props.trafficFines.trafficFines[key]
    if (fine) {
      return (
        <ListItemTrafficFine
          trafficFine={fine}
          key={index}
          cardIndex={index}
          selected={this.state.selectedCardIndex.item}
          hovered={this.state.hoveredCardIndex.item}
          onPress={this.handleItemPress}
          onLongPress={this.handleItemLongPress}
        />
      )
    }

    return null
  }

  render () {
    const hasIndex = this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0
    const smallHeader = hasIndex || (this.props.trafficFines.query && Array.isArray(this.props.trafficFines.searchResults))
    return (
      <FlatList
        ref={(list) => { this.list = list }}
        data={this.state.data}
        renderHeader={this.renderHeader}
        header={this.renderHeader()}
        ListHeaderComponent={this.renderHeader()}
        renderItem={this.renderItem}
        extraData={this.state}
        keyExtractor={this.keyExtractor}
        renderScrollComponent={(props) => {
          delete props.renderScrollComponent
          return (
            <ParallaxScrollView
              {...props}
              contentContainerStyle={styles.mainContainer}
              headerBackgroundColor={Colors.charcoal}
              contentBackgroundColor={Colors.steel}
              parallaxHeaderHeight={smallHeader ? Metrics.toolbarHeight : Metrics.parallaxHeaderHeight}
              stickyHeaderHeight={Metrics.toolbarHeight}
              renderForeground={() => (<Header background={Images.backgroundTrafficFines} title=''/>)}
              renderStickyHeader={() => (
                <View style={{height: Metrics.toolbarHeight, backgroundColor: 'black'}}/>
              )}
              renderFixedHeader={() => (
                <NavBar
                  containerStyle={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0
                  }}
                  style={{
                    container: {
                      marginTop: Metrics.toolbarPaddingTop,
                      backgroundColor: Colors.transparent,
                    }
                  }}
                  leftElement='arrow-back'
                  onLeftElementPress={this.backAction}
                  centerElement={hasIndex ? 'Traffic Fines Listing' : 'Traffic Fines'}
                  searchable={hasIndex ? null : {
                    onSearchPressed: () => { this.props.onChangeQuery('')},
                    onSearchClosed: () => { this.props.onChangeQuery('') },
                    onSubmitEditing: () => { },

                    onChangeText: this.props.onChangeQuery,
                    autoFocus: true,
                    autoCorrect: false,
                    placeholder: 'Search'
                  }}
                  isSearchActive={false}
                />
              )}
            />
          )
        }}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  trafficFines: makeSelectTrafficFines()
})

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeQuery: (query) => dispatch(Actions.queryChangeAction(query))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect([
    {
      path: 'traffic_fines'
    },
    {
      path: 'traffic_fines_categories'
    },

    {
      path: 'meta_traffic_fines'
    },
    {
      path: 'meta_traffic_fines_categories'
    }
  ])
)(TrafficFinesScreen)
