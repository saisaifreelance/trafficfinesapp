import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { createStructuredSelector } from 'reselect'
import { SectionList, LayoutAnimation, View, Text } from 'react-native'
import PropTypes from 'prop-types'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import Actions, { makeSelectEmergencyServices } from '../Redux/EmergencyServicesRedux'

import NavBar from '../Components/NavBar'
import Header from '../Components/Header'
import { Images, Colors, Metrics } from '../Themes'
import styles from './Styles/EmergencyServicesScreenStyle'
import ListItemEmergencyServiceCategory from '../Components/ListItemEmergencyServiceCategory'
import ListItemEmergencyService from '../Components/ListItemEmergencyService'

const {Types, Properties} = LayoutAnimation

const TRANSITION_DURATION = 300

class EmergencyServicesScreen extends Component {
  // // Prop type warnings
  static propTypes = {
    emergencyServices: PropTypes.shape({}).isRequired
  }

  constructor (props) {
    super(props)

    this.onBackPressed = this.onBackPressed.bind(this)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.handleItemPress = this.handleItemPress.bind(this)
    this.handleItemLongPress = this.handleItemLongPress.bind(this)
    this.renderItem = this.renderItem.bind(this)
    this.renderSectionHeader = this.renderSectionHeader.bind(this)
    this.getItems = this.getItems.bind(this)

    this.state = {
      selectedCardIndex: {
        cat: null,
        item: null,
      },
      hoveredCardIndex: {
        cat: null,
        item: null,
      },
      animating: false
    }

    this._PRESET = LayoutAnimation.create(
      TRANSITION_DURATION, Types.easeInEaseOut, Properties.opacity
    )
  }

  emergencyServiceAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'EmergencyServiceScreen',
    params: {}
  }

  componentDidUpdate (prevProps) {
    if (this.props.emergencyServices.query && this.props.emergencyServices.query !== prevProps.emergencyServices.query) {
      this.list.scrollToLocation({
        sectionIndex: 0,
        itemIndex: 0
      })
    }
  }

  handleItemPress (cardId, id) {
    this.emergencyServiceAction.params.id = id
    this.props.navigation.dispatch(this.emergencyServiceAction)
  }

  handleItemLongPress (cardId, id) {
    this.emergencyServiceAction.params.id = id
    this.props.navigation.dispatch(this.emergencyServiceAction)
  }

  handlePress (cardId) {
    if (this.state.selectedCardIndex.cat === cardId && this.state.hoveredCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
    })
    // this.list.scrollTo({y: 0, x: 0, animated: true})
    this.setState({
      selectedCardIndex: {
        cat: cardId,
        item: null
      },
      hoveredCardIndex: {
        cat: null,
        item: null
      },
      animating: true
    })
    if (this.props.onPress) this.props.onPress()
  }

  handleLongPress (cardId) {
    if (this.state.hoveredCardIndex.cat === cardId && this.state.selectedCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
    })
    this.setState({
      selectedCardIndex: {
        cat: null,
        item: null
      },
      hoveredCardIndex: {
        cat: cardId,
        item: null
      },
      animating: true,
    })
  }

  renderSectionHeader ({section: {data, title}}) {
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>
    )
  }

  renderItem ({item: {key, selected, hovered}, index: cardIndex, sepators}) {
    const category = this.props.emergencyServices.emergencyServicesCategories[key]
    if (category) {
      return (
        <ListItemEmergencyServiceCategory
          category={category}
          cardIndex={cardIndex}
          selected={selected ? cardIndex : this.state.selectedCardIndex.cat}
          hovered={hovered ? cardIndex : this.state.hoveredCardIndex.cat}
          onPress={this.handlePress}
          onLongPress={this.handleLongPress}
        />
      )
    }

    const service = this.props.emergencyServices.emergencyServices[key]
    if (service) {
      return (
        <ListItemEmergencyService
          emergencyService={service}
          key={cardIndex}
          cardIndex={cardIndex}
          selected={this.state.selectedCardIndex.item}
          hovered={this.state.hoveredCardIndex.item}
          onPress={this.handleItemPress}
          onLongPress={this.handleItemLongPress}
        />
      )
    }
    return null
  }

  getItems () {
    const {emergencyServices, emergencyServicesCategories, isSearching, query, searchResults} = this.props.emergencyServices
    const {selectedCardIndex, animating} = this.state
    if (!emergencyServices || !emergencyServicesCategories) {
      return []
    }
    let title = 'Emergency services categories'
    if (isSearching) {
      title = isSearching
    } else if (query && Array.isArray(searchResults)) {
      title = `Found ${searchResults.length} results for your query "${query}"`
      return [
        {
          data: searchResults.map((service) => ({key: service.id})),
          title
        }
      ]
    }
    const categoryKeys = Object.keys(emergencyServicesCategories)
    if (selectedCardIndex.cat || selectedCardIndex.cat === 0) {
    // if (!animating && (selectedCardIndex.cat || selectedCardIndex.cat === 0)) {
      const categoryKey = categoryKeys[selectedCardIndex.cat]
      const category = emergencyServicesCategories[categoryKey]
      return [
        {
          data: [{
            key: categoryKey,
            selected: true,
            hovered: true
          }].concat(Object.keys(category.emergency_services).map((key) => ({key}))),
          title
        }
      ]
    }
    return [
      {
        data: categoryKeys.map((key) => ({key})),
        title
      }
    ]
  }

  onBackPressed () {
    if (this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0) {
      return this.setState({selectedCardIndex: {cat: null, item: null}})
    }
    this.props.navigation.goBack()
  }

  render () {
    const hasIndex = this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0
    const smallHeader = hasIndex || (this.props.emergencyServices.query && Array.isArray(this.props.emergencyServices.searchResults))
    const sections = this.getItems()
    return (
      <SectionList
        ref={(list) => { this.list = list }}
        sections={sections}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderSectionHeader}
        renderScrollComponent={(props) => {
          delete props.renderScrollComponent
          return (
            <ParallaxScrollView
              {...props}
              contentContainerStyle={styles.mainContainer}
              headerBackgroundColor={Colors.charcoal}
              contentBackgroundColor={Colors.steel}
              parallaxHeaderHeight={smallHeader ? Metrics.toolbarHeight : Metrics.parallaxHeaderHeight}
              stickyHeaderHeight={Metrics.toolbarHeight}
              renderForeground={() => (<Header background={Images.backgroundEmergencyServices} title=''/>)}
              renderStickyHeader={() => (
                <View style={{height: Metrics.toolbarHeight, backgroundColor: 'black'}}/>
              )}
              renderFixedHeader={() => (
                <NavBar
                  containerStyle={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0
                  }}
                  style={{
                    container: {
                      marginTop: Metrics.toolbarPaddingTop,
                      backgroundColor: Colors.transparent,
                    }
                  }}
                  leftElement='arrow-back'
                  onLeftElementPress={this.onBackPressed}
                  centerElement={hasIndex ? 'Emergency Services List' : 'Emergency Services'}
                  searchable={hasIndex ? null : {
                    onSearchPressed: () => { this.props.onChangeQuery('')},
                    onSearchClosed: () => { this.props.onChangeQuery('') },
                    onSubmitEditing: () => { alert('search') },

                    onChangeText: this.props.onChangeQuery,
                    autoFocus: true,
                    autoCorrect: false,
                    placeholder: 'Search'
                  }}
                  isSearchActive={false}
                />
              )}
            />
          )
        }}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  emergencyServices: makeSelectEmergencyServices()
})

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeQuery: (query) => dispatch(Actions.queryChangeAction(query))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect([
    {
      path: '/meta_emergency_services_categories'
    },
    {
      path: '/emergency_services_categories'
    },
    {
      path: '/emergency_services'
    },
    {
      path: '/meta_emergency_services'
    }
  ])
)(EmergencyServicesScreen)
