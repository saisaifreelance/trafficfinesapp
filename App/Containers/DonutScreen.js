import React, { Component } from 'react'
import { View, Image, Modal, BackHandler, Platform, Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, { makeSelectProfile } from '../Redux/ProfileRedux'
import Donut from '../Components/Donut'
import IconButton from '../Components/IconButton'
import NavBar from '../Components/NavBar'
import ModalMenu from '../Components/ModalMenu'

// Styles
import styles from './Styles/DonutScreenStyle'
import { Images, Colors, Metrics } from '../Themes'

class DonutScreen extends Component {
  constructor (props) {
    super(props)
    this.onPress = this.onPress.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.openDrawer = this.openDrawer.bind(this)
    this.closeDrawer = this.closeDrawer.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.drawerRef = this.drawerRef.bind(this)
    this.getActions = this.getActions.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      modal: false,
      screen: null,
      actions: null
    }
  }

  static navigationOptions = {
    drawerLabel: 'Drawer',
    drawerIcon: ({tintColor}) => (
      <IconButton
        backgroundColor={tintColor}
        color={Colors.snow}
        icon='menu'
      />
    )
  }

  drawerAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'Drawer',
    params: {}
  }

  getActions () {
    const actions = []
    if (!this.props.profile.hasRated) {
      actions.push({
        title: 'Rate Us',
        onPress: () => {
          this.modal.dispatch({
            type: NavigationActions.NAVIGATE,
            routeName: 'ModalRate',
            params: {}
          })
        }
      })
    }
    if (!this.props.profile.isActivated) {
      actions.push(
        {
          title: 'Pay for App',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalPay',
              params: {}
            })
          }
        },
        {
          title: 'Activate App',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalActivate',
              params: {}
            })
          }
        }
      )
    }
    actions.push({
      title: 'Request Assistance',
      onPress: () => {
        this.modal.dispatch({
          type: NavigationActions.NAVIGATE,
          routeName: 'ModalConnect',
          params: {
            title: 'Request Assistance',
            description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
            message: 'Hi, I\'m a Road Rules App user, requesting assistance',
            contact: {
              name: 'Road Rules App',
              phone: {
                office: {
                  label: 'office',
                  value: '+263718384668'
                }
              },
              whatsapp: {
                office: {
                  label: 'office',
                  value: '+263718384668'
                }
              },
              messenger: {
                office: {
                  label: 'office',
                  value: 'RoadRulesApp'
                }
              },
            }
          }
        })
      }
    })
    this.setState({actions})
  }

  componentDidMount () {
    this.getActions()
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.screen && !prevState.screen) {
      // this.props.navigation.navigate(this.state.screen)
      const timeout = setTimeout(() => {
        this.props.navigation.navigate(this.state.screen)
        this.setState({screen: null})
        clearTimeout(timeout)
      })
    }

    if (this.props.profile.hasRated !== prevProps.profile.hasRated || this.props.profile.isActivated !== prevProps.profile.isActivated) {
      this.getActions()
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    Alert.alert(
      'Exit App',
      'Are you sure you want to exit the Road Rules App?',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'Exit', onPress: () => { BackHandler.exitApp() }},
      ],
      { cancelable: false }
    )
    return true
  }

  onPress (side) {
    switch (side) {
      case 'left':
        return this.props.navigation.navigate('EmergencyServicesScreen')
      case 'right':
        return this.props.navigation.navigate('TrafficFinesScreen')
      default:
        return
    }
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const { state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        const modalAction = {
          type: NavigationActions.NAVIGATE,
          routeName: 'ModalMenu',
          params: {
            actions: this.state.actions,
            title: 'Select an option below',
            close: this.closeModal
          }
        }

        modal.dispatch(NavigationActions.setParams({
          params: modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  drawerRef (drawer) {
    if (!this.drawer && this.state.drawer) {
      drawer.dispatch(this.drawerAction)
    }
    this.drawer = drawer
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        const modalAction = {
          type: NavigationActions.NAVIGATE,
          routeName: 'ModalMenu',
          params: {
            actions: this.state.actions,
            title: 'Select an option below',
            close: this.closeModal
          }
        }

        this.modal.dispatch(NavigationActions.setParams({
          params: modalAction.params,
          key
        }))
      }
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  openDrawer () {
    this.props.navigation.navigate('DrawerOpen')
    // this.setState({drawer: true})
  }

  closeDrawer () {
    // this.setState({drawer: false})
  }

  render () {
    const { screen } = this.state
    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalMenu ref={this.modalRef} />
      </Modal>
    ) : null
    return (
      <View style={[styles.mainContainer]}>
        <Image style={[styles.backgroundImage]} resizeMode='stretch' source={Images.backgroundHarare}/>
        <View style={styles.backgroundOverlay}/>
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.transparent,
            }
          }}
          leftElement='menu'
          rightElement='more-vert'
          onLeftElementPress={this.openDrawer}
          onRightElementPress={this.openModal}
          centerElement='Traffic Fines App'
        />
        <View style={styles.containerCentered}>
          <Donut onPress={this.onPress}/>
        </View>
        {modal}
      </View>
    )
  }
}

/*

<ModalMenu
  screenProps={{
    open: this.openModal,
    close: this.closeModal,
    isOpen: this.state.modal
  }}
  actions={[
    {
      title: 'Rate Us',
      onPress: () => {}
    },
    {
      title: 'Pay for App',
      onPress: () => {}
    },
    {
      title: 'Activate App',
      onPress: () => {}
    },
    {
      title: 'Request Assistance',
      onPress: () => {}
    },
  ]}
  title='Menu'
  description='Select an option below'
/>
*/

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(Actions.logoutRequestAction())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DonutScreen)
