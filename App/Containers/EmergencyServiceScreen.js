import React, { Component } from 'react'
import { ScrollView, Image, View, StatusBar } from 'react-native'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { makeSelectEmergencyService } from '../Redux/EmergencyServicesRedux'
import NavBar from '../Components/NavBar'
import HeaderEmergencyService from '../Components/HeaderEmergencyService'
import { Images, Colors, Metrics } from '../Themes'

// Styles
import styles from './Styles/EmergencyServiceScreenStyle'

class EmergencyServiceScreen extends Component {
  constructor (props) {
    super(props)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.state = {
      hovered: false,
      selected: false
    }
  }

  handlePress () {
    const selected = !this.state.selected
    this.setState({hovered: false, selected})
  }

  handleLongPress () {
    const hovered = !this.state.hovered
    this.setState({hovered, selected: false})
  }

  render () {
    return (
      <View style={styles.container}>
        <Image style={styles.backgroundImage} resizeMode='cover' source={Images.backgroundChat} />
        <StatusBar
          backgroundColor={Colors.snow}
          barStyle="dark-content"
        />
        <NavBar
          containerStyle={{
            backgroundColor: Colors.snow
          }}
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop
            },
            leftElement: {
              color: Colors.charcoal
            },
            rightElement: {
              color: Colors.charcoal
            },
            titleText: {
              color: Colors.charcoal
            }
          }}
          leftElement='arrow-back'
          centerElement='Emergency Service'
          onLeftElementPress={() => { this.props.navigation.goBack() }}
        />
        <ScrollView style={styles.container}>
          <HeaderEmergencyService
            emergencyService={this.props.emergencyServices.emergencyService}
            selected={this.state.selected}
            hovered={this.state.hovered}
            onPress={this.handlePress}
            onLongPress={this.handleLongPress}
          />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return createStructuredSelector({
    emergencyServices: makeSelectEmergencyService(props.id)
  })
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => [])
)(EmergencyServiceScreen)
