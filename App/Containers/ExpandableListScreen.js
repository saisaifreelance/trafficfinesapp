import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import { Colors, Metrics} from '../Themes'
import styles from './Styles/ExpandableListScreenStyle'
import ExpandableList from '../Components/ExpandableList'
import ExpandableListItem from '../Components/ExpandableListItem'

const fine = {
  id: '-Krg8Usn-FDEbrmcUVHx',
  title: 'LICENCING OFFENCES',
  traffic_fines: {
    '-Krg8UsqHtjRY0ok-8Ij': true,
    '-Krg8UsoNVq7dEiZH9iZ': true,
    '-Krg8Usrrmnhq016Mv5R': true,
    '-Krg8UspoOVI1JPgfpXk': true,
    '-Krg8Usn-FDEbrmcUVHy': true,
    '-Krg8Usn-FDEbrmcUVI-': true,
    '-Krg8UsqHtjRY0ok-8Ik': true,
    '-Krg8UsoNVq7dEiZH9iX': true,
    '-Krg8Usrrmnhq016Mv5S': true,
    '-Krg8UspoOVI1JPgfpXl': true,
    '-Krg8UssUZwgq_3l81RU': true,
    '-Krg8UsoNVq7dEiZH9i_': true,
    '-Krg8Usn-FDEbrmcUVHz': true,
    '-Krg8UsqHtjRY0ok-8Ii': true,
    '-Krg8UsoNVq7dEiZH9iY': true,
    '-Krg8Usrrmnhq016Mv5Q': true,
    '-Krg8UssUZwgq_3l81RV': true,
    '-Krg8UspoOVI1JPgfpXm': true
  },
  views: 0,
  text: '**VEHICLE REGISTRATION AND LICENCING ACT, (CHAPTER 13:14)**'
}

class ExpandableListScreen extends Component {
  render () {
    return (
      <ExpandableList
        height={Metrics.screenHeight}
        width={Metrics.screenWidth}
        backgroundColor='#f8f8f8'
        hoverOffset={25}
        transitionDuration={600}
      >
        <ExpandableListItem
          item={fine}
        />
        <ExpandableListItem
          item={fine}
        />
        <ExpandableListItem
          item={fine}
        />
        <ExpandableListItem
          item={fine}
        />
        <ExpandableListItem
          item={fine}
        />
      </ExpandableList>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpandableListScreen)
