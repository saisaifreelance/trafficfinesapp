import React, { Component } from 'react'
import { TouchableOpacity, Image, View, Text, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { View as AnimatableView } from 'react-native-animatable'
import Icon from 'react-native-vector-icons/MaterialIcons'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, {makeSelectMessage} from '../Redux/FcmRedux'

// Styles
import styles from './Styles/HeadsUpNotificationStyle'
import { Metrics, Colors } from '../Themes'

class HeadsUpNotification extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  componentDidMount () {
    console.tron.log('======== MOUNT')
    console.tron.log(this.props)
    if (this.props.message) {
      console.tron.log(`======== SET MOUNT TIMEOUT: ${Date.now()}`)
      this.timeout = setTimeout(() => {
        console.tron.log('========= CLEAR MESSAGE')
        console.tron.log(`========= CLEAR MESSAGE IN MOUNT: ${Date.now()}`)
        this.props.clearMessage()
        clearTimeout(this.timeout)
      }, 10000)
    }
  }

  componentDidUpdate (prevProps, prevState) {
    console.tron.log('======== UPDATE ??')
    console.tron.log(prevProps)
    console.tron.log(this.props)
    if ((!prevProps.message && this.props.message) || (this.props.message && prevProps.message && this.props.message.id !== prevProps.message.id)) {
      console.tron.log(`======== SET UPDATE TIMEOUT: ${Date.now()}`)
      console.tron.log(prevProps)
      console.tron.log(this.props)

      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => {
        console.tron.log(`========= CLEAR MESSAGE IN UPDATE: ${Date.now()}`)
        this.props.clearMessage()
        clearTimeout(this.timeout)
      }, 10000)
    }

    if (prevProps.message && !this.props.message) {
      clearTimeout(this.timeout)
    }
  }

  componentWillUnmount () {
    clearTimeout(this.timeout)
  }

  render () {
    console.tron.log('========= PROPS')
    console.tron.log(this.props)
    let statusBar = null
    let content = null

    if (this.props.message) {
      const { message: { image, title, description, action }, dispatch } = this.props
      statusBar = (<StatusBar hidden={true} />)
      content = (
        <TouchableOpacity onPress={() => { dispatch(action)}}>
          <View style={styles.contentContainer}>
            <View style={styles.iconContainer}>
              <Icon
                style={{position: 'absolute'}}
                name='account-circle'
                size={Metrics.icons.large}
                color={Colors.steel}
                />
              <Image style={styles.icon} source={{uri: image}} />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.title} numberOfLines={1}>{title}</Text>
              <Text style={styles.description} numberOfLines={1}>{description}</Text>
            </View>
          </View>
          <View style={styles.pullDownContainer}>
            <View style={styles.pullDown} />
          </View>
        </TouchableOpacity>
      )
    }
    return (
      <View style={{position: 'absolute', top: 0, right: 0, left: 0}}>
        {statusBar}
        <AnimatableView style={styles.container} animation={this.props.message ? 'slideInDown' : 'slideOutUp'}>
            {content}
        </AnimatableView>
    </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  message: makeSelectMessage()
})

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    clearMessage: () => dispatch(Actions.clearMessage()),
    clearMessages: () => dispatch(Actions.clearMessages()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeadsUpNotification)
