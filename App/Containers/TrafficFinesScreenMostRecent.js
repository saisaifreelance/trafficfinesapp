import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { createStructuredSelector } from 'reselect'
import { SectionList, LayoutAnimation, View, Text, BackHandler, Platform } from 'react-native'
import PropTypes from 'prop-types'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import Actions, { makeSelectTrafficFines } from '../Redux/TrafficFinesRedux'

import NavBar from '../Components/NavBar'
import Header from '../Components/Header'
import { Images, Colors, Metrics } from '../Themes'
import styles from './Styles/TrafficFinesScreenStyle'
import ListItemTrafficFineCategory from '../Components/ListItemTrafficFineCategory'
import ListItemTrafficFine from '../Components/ListItemTrafficFine'
// import ProgressModal from '../Components/ProgressModal'

const {Types, Properties} = LayoutAnimation

const TRANSITION_DURATION = 300

class TrafficFinesScreen extends Component {
  // // Prop type warnings
  static propTypes = {
    trafficFines: PropTypes.shape({}).isRequired,
  }

  constructor (props) {
    super(props)

    this.backAction = this.backAction.bind(this)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.handleItemPress = this.handleItemPress.bind(this)
    this.handleItemLongPress = this.handleItemLongPress.bind(this)
    this.renderItem = this.renderItem.bind(this)
    this.renderSectionHeader = this.renderSectionHeader.bind(this)
    this.getItems = this.getItems.bind(this)

    this.state = {
      selectedCardIndex: {
        cat: null,
        item: null,
      },
      hoveredCardIndex: {
        cat: null,
        item: null,
      },
      animating: false
    }

    this._PRESET = LayoutAnimation.create(
      TRANSITION_DURATION, Types.easeInEaseOut, Properties.opacity
    )
  }

  trafficFineAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'TrafficFineScreen',
    params: {}
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentDidUpdate (prevProps) {
    if (this.props.trafficFines.query && (this.props.trafficFines.query !== prevProps.trafficFines.query) && this.list) {
      this.list.scrollToLocation({
        sectionIndex: 0,
        itemIndex: 0,
      })
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  handleItemPress (cardId, id) {
    this.trafficFineAction.params.id = id
    this.props.navigation.dispatch(this.trafficFineAction)
  }

  handleItemLongPress (cardId, id) {
    this.trafficFineAction.params.id = id
    this.props.navigation.dispatch(this.trafficFineAction)
  }

  handlePress (cardId) {
    if (this.state.selectedCardIndex.cat === cardId && this.state.hoveredCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
    })
    // this.list.scrollTo({y: 0, x: 0, animated: true})
    this.setState({
      selectedCardIndex: {
        cat: cardId,
        item: null
      },
      hoveredCardIndex: {
        cat: null,
        item: null
      },
      animating: true
    })
    if (this.props.onPress) this.props.onPress()
  }

  handleLongPress (cardId) {
    if (this.state.hoveredCardIndex.cat === cardId && this.state.selectedCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
      alert('animation complete')
    })
    this.setState({
      selectedCardIndex: {
        cat: null,
        item: null
      },
      hoveredCardIndex: {
        cat: cardId,
        item: null
      },
      animating: true,
    })
  }

  renderTrafficFine (trafficFine, cardIndex) {
    return (
      <ListItemTrafficFine
        trafficFine={trafficFine}
        key={cardIndex}
        cardIndex={cardIndex}
        selected={this.state.selectedCardIndex.item}
        hovered={this.state.hoveredCardIndex.item}
        onPress={this.handleItemPress}
        onLongPress={this.handleItemLongPress}
      />
    )
  }

  renderTrafficFineCategory (category, cardIndex) {
    return (
      <ListItemTrafficFineCategory
        category={category}
        cardIndex={cardIndex}
        selected={this.state.selectedCardIndex.cat}
        hovered={this.state.hoveredCardIndex.cat}
        onPress={this.handlePress}
        onLongPress={this.handleLongPress}
      />
    )
  }

  renderSectionHeader ({section: {data, title}}) {
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>
    )
  }

  renderItem ({item: {key, selected, hovered}, index: cardIndex, sepators}) {
    const category = this.props.trafficFines.trafficFinesCategories[key]
    if (category) {
      return (
        <ListItemTrafficFineCategory
          category={category}
          cardIndex={cardIndex}
          selected={selected ? cardIndex : this.state.selectedCardIndex.cat}
          hovered={hovered ? cardIndex : this.state.hoveredCardIndex.cat}
          onPress={this.handlePress}
          onLongPress={this.handleLongPress}
        />
      )
    }

    const fine = this.props.trafficFines.trafficFines[key]
    if (fine) {
      return (
        <ListItemTrafficFine
          trafficFine={fine}
          key={cardIndex}
          cardIndex={cardIndex}
          selected={this.state.selectedCardIndex.item}
          hovered={this.state.hoveredCardIndex.item}
          onPress={this.handleItemPress}
          onLongPress={this.handleItemLongPress}
        />
      )
    }
    return null
  }

  getItems () {
    const {trafficFines, trafficFinesCategories, isSearching, query, searchResults} = this.props.trafficFines
    const {selectedCardIndex, animating} = this.state
    if (!trafficFines || !trafficFinesCategories) {
      return []
    }
    let title = 'Traffic fines categories'
    if (isSearching) {
      title = isSearching
    } else if (query && Array.isArray(searchResults)) {
      title = `Found ${searchResults.length} results for your query "${query}"`
      return [
        {
          data: searchResults.map((fine) => ({key: fine.id})),
          title
        }
      ]
    }
    const categoryKeys = Object.keys(trafficFinesCategories)
    // if (!animating && (selectedCardIndex.cat || selectedCardIndex.cat === 0)) {
    if (selectedCardIndex.cat || selectedCardIndex.cat === 0) {
      const categoryKey = categoryKeys[selectedCardIndex.cat]
      const category = trafficFinesCategories[categoryKey]
      return [
        {
          data: [{
            key: categoryKey,
            selected: true,
            hovered: false
          }].concat(Object.keys(category.traffic_fines).map((key) => ({key}))),
          title
        }
      ]
    }
    return [
      {
        data: categoryKeys.map((key) => ({key})),
        title
      }
    ]
  }

  backAction () {
    if (this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0) {
      this.setState({selectedCardIndex: {cat: null, item: null}})
      return true
    }
    this.props.navigation.goBack()
    return true
  }

  render () {
    const hasIndex = this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0
    const smallHeader = hasIndex || (this.props.trafficFines.query && Array.isArray(this.props.trafficFines.searchResults))
    const sections = this.getItems()
    return (
      <SectionList
        ref={(list) => this.list = list}
        data={sections[0] ? sections[0].data : null}
        sections={sections}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderSectionHeader}
        renderScrollComponent={(props) => {
          delete props.renderScrollComponent
          return (
            <ParallaxScrollView
              {...props}
              contentContainerStyle={styles.mainContainer}
              headerBackgroundColor={Colors.charcoal}
              contentBackgroundColor={Colors.steel}
              parallaxHeaderHeight={smallHeader ? Metrics.toolbarHeight : Metrics.parallaxHeaderHeight}
              stickyHeaderHeight={Metrics.toolbarHeight}
              renderForeground={() => (<Header background={Images.backgroundTrafficFines} title=''/>)}
              renderStickyHeader={() => (
                <View style={{height: Metrics.toolbarHeight, backgroundColor: 'black'}}/>
              )}
              renderFixedHeader={() => (
                <NavBar
                  containerStyle={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0
                  }}
                  style={{
                    container: {
                      marginTop: Metrics.toolbarPaddingTop,
                      backgroundColor: Colors.transparent,
                    }
                  }}
                  leftElement='arrow-back'
                  onLeftElementPress={this.backAction}
                  centerElement={hasIndex ? 'Traffic Fines Listing' : 'Traffic Fines'}
                  searchable={hasIndex ? null : {
                    onSearchPressed: () => { this.props.onChangeQuery('')},
                    onSearchClosed: () => { this.props.onChangeQuery('') },
                    onSubmitEditing: () => { alert('search') },

                    onChangeText: this.props.onChangeQuery,
                    autoFocus: true,
                    autoCorrect: false,
                    placeholder: 'Search'
                  }}
                  isSearchActive={false}
                />
              )}
            />
          )
        }}
      />
    )
  }
}

const mapStateToProps = createStructuredSelector({
  trafficFines: makeSelectTrafficFines()
})

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeQuery: (query) => dispatch(Actions.queryChangeAction(query))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect([
    {
      path: 'traffic_fines'
    },
    {
      path: 'traffic_fines_categories'
    },

    {
      path: 'meta_traffic_fines'
    },
    {
      path: 'meta_traffic_fines_categories'
    }
  ])
)(TrafficFinesScreen)
