import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { equals } from 'ramda'
import { KeyboardAvoidingView, View, StatusBar, BackHandler, Platform, Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import ImagePicker from 'react-native-image-crop-picker'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, { makeSelectProfile } from '../Redux/ProfileRedux'
import ProfileReducer from '../Types/ProfileReducer'
import Form from '../Components/Form'
import KeyboardDismiss from '../Components/KeyboardDismiss'
import NavBar from '../Components/NavBar'
import ProgressModal from '../Components/ProgressModal'
import ToolbarLeftElementText from '../Components/ToolbarLeftElementText'
import HeaderLogo from '../Components/HeaderLogo'
import ProfilePictureEdit from '../Components/ProfilePictureEdit'
import { fields } from '../Schema/Profile'
import { Colors, Fonts, Metrics, Images } from '../Themes'
import { getFirebase } from 'react-redux-firebase'

// Styles
import styles from './Styles/ProfileScreenStyle'

class ProfileScreen extends Component {
  static propTypes = {
    profile: ProfileReducer.isRequired,
    changeField: PropTypes.func.isRequired,
    clearError: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    updateProfilePicture: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this.changeField = this.changeField.bind(this)
    this.goHome = this.goHome.bind(this)
    this.openCamera = this.openCamera.bind(this)
    this.openPicker = this.openPicker.bind(this)
    this.backAction = this.backAction.bind(this)
  }

  homeAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'HomeScreen',
    params: {}
  }

  componentDidMount () {
    const {data} = this.props.profile
    const defaultData = {}
    if (data) {
      for (let i = 0; i < fields.length; i += 1) {
        const key = fields[i].key
        if (data[key]) {
          defaultData[key] = data[key]
        }
      }
      if (Object.keys(defaultData).length) {
        this.props.changeField(defaultData)
      } else {
        // this.props.changeField({ location: 'manicaland', name: 'Jay Boss' })
      }
    }
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentDidUpdate (prevProps) {
    /*

        const {profile} = this.props.profile
        const defaultData = {}
        if ((profile && typeof profile === 'object') && !prevProps.profile) {
          for (let i = 0; i < fields.length; i += 1) {
            const key = fields[i].key
            if (profile[key]) {
              defaultData[key] = profile[key]
            }
          }
          this.props.changeField(defaultData)
        }
    */

    const {data} = this.props.profile
    const defaultData = {}
    if (data && !equals(data, prevProps.profile.data)) {
      for (let i = 0; i < fields.length; i += 1) {
        const key = fields[i].key
        if (data[key]) {
          defaultData[key] = data[key]
        }
      }
      this.props.changeField(defaultData)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    if (this.props.profile.hasProfile) {
      this.goHome()
      return true
    }

    Alert.alert(
      'Exit App',
      'Are you sure you want to exit the Road Rules App before completing your profile? You will be asked to do so next time.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'Exit', onPress: () => { BackHandler.exitApp() }},
      ],
      { cancelable: false }
    )
    return true
  }

  changeField (field, value) {
    this.props.changeField({[field]: value})
  }

  goHome () {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        this.homeAction
      ]
    }))
  }

  openPicker () {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true
    }).then(image => {
      const {height, mime, path, size, width, data, localIdentifier, filename, sourceURL} = image
      console.log(image)
      this.props.updateProfilePicture(image)
    }).catch((e) => {
    })
  }

  openCamera () {
    ImagePicker.openCamera({
      width: 400,
      height: 400,
      cropping: true
    }).then(image => {
    }).catch((e) => {
    })
  }

  render () {
    let navbar = null
    let isFetching = null
    const style = {paddingTop: 20}
    if (this.props.profile.hasProfile) {
      style.paddingTop = Metrics.toolbarHeight
      navbar = (
        <NavBar
          containerStyle={{
            backgroundColor: Colors.silver,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            borderBottomColor: Colors.steel,
            borderBottomWidth: 1
          }}
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.transparent
              // backgroundColor: Colors.steel
            },
            titleText: {
              ...Fonts.style.h5,
              color: Colors.charcoal,
            },
          }}
          leftElement={(
            <ToolbarLeftElementText onPress={this.goHome} icon="keyboard-arrow-left">Home</ToolbarLeftElementText>)}
          onRightElementPress={this.goHome}
        />
      )
    }
    if (this.props.profile.isFetching) {
      isFetching = (<ProgressModal message={this.props.profile.isFetching} />)
    }
    return (
      <View style={[styles.container, style]}>
        <StatusBar
          backgroundColor={Colors.silver}
          barStyle='dark-content'
        />
        <KeyboardAvoidingView
          style={{flex: 1}}
          contentContainerStyle={{flex: 1, marginTop: Metrics.smallMargin}}
          behavior='position'
          keyboardVerticalOffset={-300}>
          <ProfilePictureEdit
            image={this.props.profile.image}
            profile_pic={this.props.profile.data ? this.props.profile.data.profile_pic : null}
            onPress={this.openPicker}/>
          <KeyboardDismiss>
            <Form
              fields={fields}
              data={this.props.profile.form}
              error={this.props.profile.error}
              clearError={this.props.clearError}
              legend={this.props.profile.hasProfile ? 'Edit your profile' : 'Complete your registration by creating a profile'}
              submitText='Save'
              onChangeField={this.changeField}
              onSubmit={this.props.submit}
            />
          </KeyboardDismiss>
        </KeyboardAvoidingView>
        {navbar}
        {isFetching}
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {
    changeField: (fields) => dispatch(Actions.fieldChangeAction(fields)),
    clearError: () => dispatch(Actions.clearErrorAction()),
    submit: (fields, form) => dispatch(Actions.updateProfileRequestAction(fields, form)),
    updateProfilePicture: (image) => dispatch(Actions.updateProfilePictureAction(image)),
    logout: () => dispatch(Actions.logoutRequestAction())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
