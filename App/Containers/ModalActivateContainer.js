import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, {makeSelectProfile} from '../Redux/ProfileRedux'

import IconButton from '../Components/IconButton'
import Button from '../Components/Button'
import TextField from '../Components/TextField'
import styles from './Styles/ModalActivateContainerStyle'

class ModalActivateContainer extends Component {
  // // Prop type warnings
  static propTypes = {
    title: PropTypes.string.isRequired,
    close: PropTypes.func
  }
  //
  // Defaults for props
  static defaultProps = {
    title: 'Please enter the activation code you received via SMS after you paid for the app.',
  }

  constructor (props) {
    super(props)
    this.onSubmitEditing = this.onSubmitEditing.bind(this)
    this.onChangeText = this.onChangeText.bind(this)
    this.state = {
      activationCode: '',
      error: '',
      message: '',
    }
    this.close = this.close.bind(this)
  }

  onChangeText (activationCode) {
    this.setState({activationCode})
  }

  onSubmitEditing () {
    if (this.props.profile.data.activationCode === this.state.activationCode) {
      return this.props.activate(this.state.activationCode)
    }
    this.setState({error: 'The activation code you have entered is incorrect, please contact Request Assistance from Road Rules App Support team if this problem persists'})
  }

  close () {
    if (this.props.close) {
      return this.props.close()
    }
    this.props.navigation.goBack()
  }

  componentDidMount () {
    if (this.props.profile.isActivated) {
      this.setState({error: '', message: `You have successfully activated Road Rules App, full version, thanks ${this.props.profile.data.name}!`})
    }
  }

  componentDidUpdate (prevProps) {
    if (!prevProps.profile.isActivated && this.props.profile.isActivated) {
      this.setState({error: '', message: `You have successfully activated Road Rules App, full version, thanks ${this.props.profile.data.name}!`})
    }
  }

  render () {
    let text = (
      <View style={styles.section}>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
    if (this.state.message) {
      text = (
        <TouchableOpacity style={styles.section} onPress={this.close}>
          <Text style={styles.title}>{this.state.message}</Text>
        </TouchableOpacity>
      )
    }
    if (this.state.error) {
      text = (
        <TouchableOpacity style={styles.section} onPress={() => { this.setState({error: ''}) }}>
          <Text style={styles.titleError}>{this.state.error}</Text>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            <View style={styles.container}>
              <View style={styles.modalControlsContainer}>
                <IconButton icon='close' onPress={this.close}/>
              </View>
              {text}
              <View style={styles.section}>
                <TextField
                  value={this.state.activationCode}
                  error=''
                  label='Activation Code'
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitEditing}
                  textInputProps={{
                    keyboardType: 'numeric'
                  }}
                />
                <Button onPress={this.onSubmitEditing}>ACTIVATE</Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {
    activate: (activationCode) => dispatch(Actions.activateAction(activationCode))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalActivateContainer)
