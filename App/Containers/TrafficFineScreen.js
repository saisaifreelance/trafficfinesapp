import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, Image, View, TouchableOpacity, StatusBar, BackHandler, Platform } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { createStructuredSelector } from 'reselect'
import { GiftedChat } from 'react-native-gifted-chat'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { generateId } from '../Services/Firebase'
import Actions, { makeSelectTrafficFine } from '../Redux/TrafficFinesRedux'
import ChatActions, { makeSelectChat, makeSelectChatRoomMessages, makeSelectChatRoomMeta, messagesPopulates, makeSelectProfile } from '../Redux/ChatRedux'
import NavBar from '../Components/NavBar'
import HeaderTrafficFine from '../Components/HeaderTrafficFine'
import ChatPrompt from '../Components/ChatPrompt'
import { Images, Colors, Metrics } from '../Themes'

const pageSize = 10

// Styles
import styles from './Styles/TrafficFineScreenStyle'

class TrafficFineScreen extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    messageChange: PropTypes.func.isRequired,
    sendMessage: PropTypes.func.isRequired,
    loadEarlierMessages: PropTypes.func.isRequired,
    chat: PropTypes.shape({
      isFetching: PropTypes.string,
      message: PropTypes.shape({
        id: PropTypes.string,
        text: PropTypes.string.isRequired,
        createdAt: PropTypes.number,
        user: PropTypes.shape({
          id: PropTypes.string,
          name: PropTypes.string,
          avatar: PropTypes.string
        })
      }).isRequired
    }).isRequired
  }
  constructor (props) {
    super(props)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      hovered: false,
      selected: false,
      // selected: false
    }
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    if (!this.state.selected) {
      this.setState({selected: true})
      return true
    }
    this.props.navigation.goBack()
    return true
  }

  handlePress () {
    const selected = !this.state.selected
    this.setState({hovered: false, selected})
  }

  handleLongPress () {
    const hovered = !this.state.hovered
    this.setState({hovered, selected: false})
  }

  componentWillMount () {}

  onSend (messages = []) {
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
  }

  render () {
    let overlay = null
    let chat = null
    let header = null
    let user = {}
    let chatPrompt = null

    try {
      user._id = this.props.profile.id
      user.name = this.props.profile.name
      user.avatar = this.props.profile.profile_pic.downloadUrl
    } catch (e) {

    }

    if (this.state.selected) {
      header = (
        <ScrollView style={styles.container}>
          <HeaderTrafficFine
            trafficFine={this.props.trafficFines.trafficFine}
            selected={this.state.selected}
            hovered={this.state.hovered}
            onPress={this.handlePress}
            onLongPress={this.handleLongPress}
          />
        </ScrollView>
      )

      overlay = (
        <TouchableOpacity
          style={{
            backgroundColor: 'rgba(0,0,0,0.5)',
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
          }}
          onPress={() => { this.setState({hovered: false, selected: false}) }}
        />
      )
      chat = null
      chatPrompt = (
        <ChatPrompt onPress={() => { this.setState({hovered: false, selected: false}) }} />
      )
    }

    if (this.state.hovered) {
      header = (
        <HeaderTrafficFine
          trafficFine={this.props.trafficFines.trafficFine}
          selected={this.state.selected}
          hovered={this.state.hovered}
          onPress={this.handlePress}
          onLongPress={this.handleLongPress}
        />
      )
      overlay = (
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
          }}
          onPress={() => { this.setState({hovered: false, selected: false}) }}
        />
      )
      chat = null
    }

    if (!this.state.selected && !this.state.hovered) {
      header = (
        <HeaderTrafficFine
          trafficFine={this.props.trafficFines.trafficFine}
          selected={this.state.selected}
          hovered={this.state.hovered}
          onPress={this.handlePress}
          onLongPress={this.handleLongPress}
        />
      )
      overlay = null

      chat = (
        <View style={styles.container}>
          <GiftedChat
            messages={this.props.messages}
            placeholder={'Type a message...'}
            messageIdGenerator={(param) => generateId(`chatRooms/${this.props.id}/messages`)}
            user={user}
            onSend={(messages) => {
              const message = Array.isArray(messages) ? messages[0] : null
              if (message) {
                this.props.sendMessage(message, this.props.trafficFines.trafficFine)
              }
            }}
            locale={'en'}
            timeFormat={'LT'}
            dateFormat={'ll'}
            isAnimated
            loadEarlier
            onLoadEarlier={this.props.loadEarlierMessages}
            isLoadingEarlier={!user}
          />
          {overlay}
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <Image style={styles.backgroundImage} resizeMode='cover' source={Images.backgroundChat} />
        <StatusBar
          backgroundColor={Colors.snow}
          barStyle="dark-content"
        />
        <NavBar
          containerStyle={{
            backgroundColor: Colors.snow
          }}
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.snow
            },
            leftElement: {
              color: Colors.charcoal
            },
            rightElement: {
              color: Colors.charcoal
            },
            titleText: {
              color: Colors.charcoal
            }
          }}
          leftElement='arrow-back'
          centerElement='Traffic Fine Explanation'
          onLeftElementPress={() => { this.props.navigation.goBack() }}
        />
        {header}
        {chatPrompt}
        {chat}
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return createStructuredSelector({
    trafficFines: makeSelectTrafficFine(props.id),
    chat: makeSelectChat(props.id),
    meta: makeSelectChatRoomMeta(props.id),
    messages: makeSelectChatRoomMessages(props.id),
    profile: makeSelectProfile()
  })
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    loadEarlierMessages: () => dispatch(ChatActions.loadEarlierMessagesAction()),
    messageChange: (text) => dispatch(ChatActions.messageChangeRequestAction(text)),
    sendMessage: (message, chat) => dispatch(ChatActions.sendMessageAction(props.id, message, chat))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => {
    const { chat: { count, firstId } } = props
    const queries = [
      {
        path: `chatRooms/${props.id}/meta`
      }
    ]
    const pageParams = ['orderByKey', `limitToLast=${count}`]
    if (firstId) {
      pageParams.push(`endAt=${firstId}`)
      queries.push({
        path: `chatRooms/${props.id}/messages`,
        queryParams: ['orderByKey', `startAt=${firstId}`, `limitToFirst=${20}`],
        populates: messagesPopulates
      })
    }
    queries.push({
      path: `chatRooms/${props.id}/messages`,
      queryParams: pageParams,
      populates: messagesPopulates
    })
    console.tron.log('>>>>> QUERIES')
    console.tron.log(queries)
    return queries
  })
)(TrafficFineScreen)
