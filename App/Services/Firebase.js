import { getFirebase, actionTypes } from 'react-redux-firebase'
import { NativeModules } from 'react-native'
import Actions from '../Redux/ProfileRedux'

const {FirebasePhoneAuth} = NativeModules

export const generateId = (path) => {
  return getFirebase().database().ref(path).push().key
}

/**
 * @description Upload a file with actions fired for progress, success, and errors
 * @param {Function} dispatch - Action dispatch function
 * @param {Object} firebase - Internal firebase object
 * @param {Object} opts - File data object
 * @param {Object} opts.path - Location within Firebase Stroage at which to upload file.
 * @param {Blob} opts.file - File to upload
 * @private
 */

export const uploadFileWithProgress = (emit, firebase, {path, file}) => {
  emit({
    type: actionTypes.FILE_UPLOAD_START,
    payload: {path, file}
  })
  // TODO: Add file.name and file.extension
  // FILE: {id, height, data, mime, width, localIdentifier, size, filename, path, sourceURL, name, extension} = image
  const uploadEvent = firebase.storage().ref(`${path}/${file.name}${file.extension ? '.' : ''}${file.extension}`).putFile(file.path)

  // TODO: Allow config to control whether progress it set to state or not
  const unListen = uploadEvent.on(
    firebase.storage.TaskEvent.STATE_CHANGED,
    {
      next: (snapshot) => {
        const percent = Math.floor(snapshot.bytesTransferred / snapshot.totalBytes * 100)
        emit({
          type: actionTypes.FILE_UPLOAD_PROGRESS,
          file,
          path,
          payload: {snapshot, percent}
        })
      },
      error: (err) => {
        emit({type: actionTypes.FILE_UPLOAD_ERROR, file, path, payload: err})
        unListen()
      },
      complete: () => {
        emit({type: actionTypes.FILE_UPLOAD_COMPLETE, file, path, payload: file})
        unListen()
      }
    }
  )
  return uploadEvent
}

/**
 * @description Upload file to Firebase Storage with option to store
 * file metadata within Firebase Database
 * @param {Function} dispatch - Action dispatch function
 * @param {Object} firebase - Internal firebase object
 * @param {Object} opts - Options object
 * @param {String} opts.path - Location within Firebase Stroage at which to upload files.
 * @param {Blob} opts.file - File Blob to be uploaded
 * @param {String} opts.dbPath - Datbase path to write file meta data to
 * @private
 */
export const uploadFile = (emit, firebase, {path, file, dbPath}) => uploadFileWithProgress(emit, firebase, {path, file})
  .then((res) => {
    if (!dbPath) {
      return res
    }

    // OG FILE: {id, height, data, mime, width, localIdentifier, size, filename, path, sourceURL, name, extension} = image
    const {downloadUrl, totalBytes, metadata: {timeCreated, updated, name}} = res
    const fileData = {
      id: file.id,
      name,
      downloadUrl,
      totalBytes,
      createdAt: new Date(timeCreated).getTime(),
      modifiedAt: new Date(updated).getTime()
    }

    return firebase.database()
      .ref(dbPath)
      .set(fileData)
      .then(snapshot => ({snapshot, key: snapshot.key, File: fileData}))
  })

const create = () => {
  const init = () => {
    const messaging = getFirebase().messaging()
    messaging.requestPermissions()
    messaging.subscribeToTopic('foobar')

    messaging.getToken()
      .then((token) => {
      })
      .catch((e) => {
      })

    messaging.onTokenRefresh((token) => {
    })

    messaging.onMessage((message) => {
    })
  }

  const requestPermissions = () => {
    return getFirebase().messaging().requestPermissions()
  }

  const getToken = () => {
    return getFirebase().messaging().getToken()
      .then((token) => token)
      .catch((e) => {
        throw new Error(e.message)
      })
  }

  const setOnTokenRefresh = (emit) => {
    return getFirebase().messaging().onTokenRefresh((...args) => {
      emit(args)
    })
  }

  const setOnMessage = (emit) => {
    return getFirebase().messaging().onMessage((message) => {
      const { aps, finish, _completionHandlerId, _finishCalled, _notificationType, opened_from_tray } = message
      emit(message)
    })
  }

  const getInitialNotification = () => {
    return getFirebase().messaging().getInitialNotification()
      .then((notification) => {
        console.tron.log('++++ Notification which opened the app: ', notification);
        console.tron.log(notification);
        return notification
      })
      .catch((e) => {
        console.tron.log('++++ Notification which opened the app error: ');
        console.tron.log(e);
      })
  }

  const signInAnonymously = () =>
    getFirebase()
      .auth()
      .signInAnonymously()
      .then((user) => {
        return user._user
      })
      .catch((error) => {
        const {code, message, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo} = error
        throw new Error(message)
      })

  const signInWithPhoneNumber = (phoneNumber) => {
    return FirebasePhoneAuth
      .verifyPhoneNumber(phoneNumber)
      .then(verificationId => {
        return verificationId
      })
      .catch((error) => {
        const {code, message, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo} = error
        throw new Error(message)
      })
  }

  const verify = (verificationCode, verificationId) => {
    return FirebasePhoneAuth
      .signInWithCredential(verificationCode, verificationId)
      .then((user) => {
        return user
      })
      .catch((error) => {
        const {code, message, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo} = error
        throw new Error(message)
      })
  }

  const updateProfile = (id, data) => {
    const root = 'drivers'
    const updates = Object.keys(data).reduce((accumulator, key) => ({
      ...accumulator,
      [`/${root}/${id}/${key}`]: data[key]
    }), {})
    updates[`/${root}/${id}/dateModified`] = Date.now()
    return getFirebase()
      .database()
      .ref()
      .update(updates).then((response) => {
        const {status} = response
        if (status !== 'success') {
          throw new Error('Profile update failed, please check your internet connection and try again')
        }
        return status
      })
      .catch((error) => {
        throw new Error(error.userInfo.NSLocalizedDescription)
      })
  }

  const updateToken = (id, token) => {
    const updates = {
      [`drivers/${id}/token`]: token,
      [`drivers/${id}/modifiedAt`]: Date.now()
    }
    return firebaseUpdate(updates, 'Failed to update Token')
      .then((response) => {
        const {status} = response
        if (response !== 'success') {
          throw new Error('Failed to update Token, please check your internet connection and try again')
        }
        return status
      })
      .catch((error) => {
        throw new Error(error.message)
      })
  }

  const updateProfilePicture = (emit, id, image) => {
    const path = 'profile_pics'
    const dbPath = 'images/profile_pics'
    const dot = image.path.lastIndexOf('.') + 1
    const extension = dot ? image.path.slice(dot).toLowerCase() : ''
    const firebase = getFirebase()
    const name = firebase.database().ref(dbPath).push().key
    const file = {...image, extension, name, id: name}
    const action = Actions.updateProfilePictureRequestAction(file)
    return Promise
      .resolve('start')
      .then(() => {
        emit(action)
        return uploadFile(emit, firebase, {
          path,
          file: {...image, extension, name, id: name},
          dbPath: `${dbPath}/${name}`
        })
      })
      .then((resp) => {
        const {snapshot, key, File} = resp
        return File
      })
      .catch((e) => {
        throw new Error(e.message)
      })
      .then((File) => firebase.ref().update({
        [`drivers/${id}/profile_pic`]: File.id,
        [`drivers/${id}/downloadUrl`]: File.downloadUrl
      }))
      .then((response) => {
        return response
      })
      .catch((e) => {
        throw new Error(e.message)
      })
  }

  const logout = () => getFirebase().logout()

  const fetchProfilePicture = (id) => getFirebase().database().ref(`images/profile_pics/${id}`).once('value').then((snapshot) => {
    const image = snapshot.val()
    if (image) {
      return image
    }
    throw new Error('Profile picture not found')
  }).catch((e) => {
    throw new Error(e.message)
  })

  const firebaseSet = (data, path, errorMessage = 'Failed to add record') => getFirebase().database().ref(path).set(data)
    .then((response) => {
      const {status} = response
      if (status !== 'success') {
        throw new Error(errorMessage)
      }
      return status
    })
    .catch((error) => {
      throw new Error(error.userInfo.NSLocalizedDescription)
    })

  const firebaseUpdate = (updates, errorMessage = 'Failed to update') => getFirebase().database().ref().update(updates)
    .then((response) => {
      const {status} = response
      if (status !== 'success') {
        throw new Error(errorMessage)
      }
      return status
    })
    .catch((error) => {
      throw new Error(error.userInfo.NSLocalizedDescription)
    })

  return {
    signInAnonymously,
    signInWithPhoneNumber,
    verify,
    updateProfile,
    updateProfilePicture,
    fetchProfilePicture,
    logout,

    firebaseSet,
    firebaseUpdate,
    updateToken,
    requestPermissions,
    getToken,
    setOnTokenRefresh,
    setOnMessage,
    getInitialNotification
  }
}

export default {
  create
}
