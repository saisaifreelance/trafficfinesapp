import { Platform, Linking, NativeModules, Share } from 'react-native'
import { AppInviteDialog } from 'react-native-fbsdk'
import DeviceInfo from 'react-native-device-info'
const {FirebaseInvites} = NativeModules

const isString = (str) => Object.prototype.toString.call(str) === '[object String]'
const isBool = (bool) => Object.prototype.toString.call(bool) === '[object Boolean]'

const create = () => {
  const openLink = (url, cb) => {
    return Linking.canOpenURL(url).then((canOpen) => {
      if (!canOpen) {
        return Promise.reject(new Error(`The URL is invalid: ${url}`))
      } else {
        return Linking.openURL(url).catch((err, other) => {
          return Promise.reject(err)
        })
      }
    })
  }

  const call = (args) => {
    const settings = Object.assign({
      prompt: true
    }, args)

    if (!settings.number) { return Promise.reject(new Error('Please provide a number to call')) }
    if (!isString(settings.number) || !isBool(settings.prompt)) { return Promise.reject(new Error('The provided arguments are not valid types')) }

    const url = `${Platform.OS === 'ios' && settings.prompt ? 'telprompt:' : 'tel:'}${settings.number}`

    return openLink(url)
  }

  const messenger = (args) => {
    const settings = Object.assign({
      prompt: true
    }, args)

    if (!settings.messenger) { return Promise.reject(new Error('Please provide a messenger id to message')) }
    if (!isString(settings.messenger) || !isString(settings.message) || !isBool(settings.prompt)) { return Promise.reject(new Error('The provided arguments are not valid types')) }

    const url = `https://m.me/${settings.messenger}?ref=${encodeURI(settings.message)}`

    return openLink(url)
  }

  const whatsapp = (args) => {
    const settings = Object.assign({
      prompt: true
    }, args)

    if (!settings.whatsapp) { return Promise.reject(new Error('Please provide a number to send a message to')) }
    if (!isString(settings.whatsapp) || !isString(settings.message) || !isBool(settings.prompt)) { return Promise.reject(new Error('The provided arguments are not valid types')) }

    const url = `whatsapp://send?phone=${settings.whatsapp}&text=${encodeURI(settings.message)}`

    return openLink(url)
  }

  const firebaseInvite = (message, title, deeplink) => {
    FirebaseInvites.invite(message, title, deeplink)
  }

  const facebookInvite = () => {
    const applinkUrl = 'https://fb.me/1745404969083735'
    const previewImageUrl = 'http://www.roadrules.co.zw/wp-content/uploads/2016/07/Screenshot_2015-09-30-15-35-34-236x412_c.png'

    if (AppInviteDialog.canShow()) {
      const content = {
        applinkUrl,
        previewImageUrl
      }
      AppInviteDialog.show(content).catch(e => {
      })
    } else {
      alert('You do not have the Facebook app installed')
    }
  }

  const share = (message, title, url, options = {}) => {
    return Share.share({message, title, url}, options)
  }

  const rate = () => {
    // const bundleId = DeviceInfo.getBundleId()
    const bundleId = 'co.neolab.trafficfines'
    const MARKET_URL = `market://details?id=${bundleId}`
    const PLAY_URL = `http://play.google.com/store/apps/details?id=${bundleId}`
    return Linking.canOpenURL(MARKET_URL).then(supported => {
      if (!supported) {
        return Linking.canOpenURL(PLAY_URL).then(supported => {
          if (!supported) {

          } else {
            Linking.openURL(PLAY_URL)
          }
        }).catch(err => console.error('An error occurred', err))

      } else {
        Linking.openURL(MARKET_URL)
      }
    }).catch(err => console.error('An error occurred', err))
  }

  return {
    create,
    call,
    whatsapp,
    messenger,
    firebaseInvite,
    share,
    facebookInvite,
    rate
  }
}

export default {
  create
}
