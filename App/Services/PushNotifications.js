import { getFirebase } from 'react-redux-firebase'

const create = () => {
  const init = () => {
    const messaging = getFirebase().messaging()
    messaging.requestPermissions()
    messaging.subscribeToTopic('foobar')

    messaging.getToken()
      .then((token) => {
      })
      .catch((e) => {
      })

    messaging.onTokenRefresh((token) => {
    })

    messaging.onMessage((message) => {
    })
  }
  const requestPermissions = () => {
    getFirebase().messaging().requestPermissions()
  }
  return {
    init,
    requestPermissions
  }
}

export default { create }
