import * as Facebook from 'react-native-fbsdk'

const applinkUrl = 'https://fb.me/1745404969083735'
const previewImageUrl = 'http://www.roadrules.co.zw/wp-content/uploads/2016/07/Screenshot_2015-09-30-15-35-34-236x412_c.png'

export const appInvite = () => {
  if (Facebook.AppInviteDialog.canShow()) {
    const content = {
      applinkUrl,
      previewImageUrl
    }
    const ret = Facebook.AppInviteDialog.show(content).catch(e => {
    })
    return ret
  }
  return Promise.reject(new Error('Facebook is not installed on your device'))
}
