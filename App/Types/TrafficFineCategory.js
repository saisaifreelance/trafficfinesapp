import PropTypes from 'prop-types'
import TrafficFine from './TrafficFine'
export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  description: PropTypes.string,
  icon: PropTypes.string,
  views: PropTypes.number.isRequired,
  traffic_fines: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.bool, TrafficFine])).isRequired
})
