import PropTypes from 'prop-types'

const field = PropTypes.shape({
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
})
const fieldArray = PropTypes.objectOf(field)

export default PropTypes.shape({
  name: PropTypes.string.isRequired,
  phone: fieldArray.isRequired,
  whatsapp: fieldArray.isRequired,
  messenger: fieldArray.isRequired
})
