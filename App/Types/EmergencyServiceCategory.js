import PropTypes from 'prop-types'
import EmergencyService from './EmergencyService'
export default PropTypes.shape({
  count: PropTypes.number.isRequired,
  emergency_services: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.bool, EmergencyService])).isRequired,
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  views: PropTypes.number.isRequired

})
