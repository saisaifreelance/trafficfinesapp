export const fields = [
  {
    title: 'Phone number',
    key: 'phoneNumber',
    type: 'phone_number',
    tableEditable: true,
    formEditable: true,
    required: true,
    validate: (value) => {
      if (typeof value !== 'string' || !value) {
        return 'Penalty is required'
      }
      return null
    }
  }
]
