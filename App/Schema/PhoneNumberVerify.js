export const fields = [
  {
    title: 'Verification Code',
    key: 'verificationCode',
    type: 'numeric',
    tableEditable: true,
    formEditable: true,
    required: true,
    validate: (value) => {
      if (typeof value !== 'string' || !value) {
        return 'Verification Code is required'
      }
      return null
    }
  }
]
