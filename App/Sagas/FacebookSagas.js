import { put, select, call } from 'redux-saga/effects'
import { is } from 'ramda'

// exported to make available for tests
export const selectState = (state) => state

export function * appInvite (facebook) {
  yield call(facebook)
}
