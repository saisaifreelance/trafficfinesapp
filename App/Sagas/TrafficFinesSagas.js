import { put, select, call } from 'redux-saga/effects'
import Actions from '../Redux/TrafficFinesRedux'

export const selectTrafficFines = (state) => state.trafficFines.trafficFines.asMutable()

export function * searchTrafficFines (search, { query }) {
  const data = yield select(selectTrafficFines)
  try {
    const results = yield call(search.search, data, query)
    yield put(Actions.queryChangeResultAction(query, results))
  } catch (e) {
  }
}
