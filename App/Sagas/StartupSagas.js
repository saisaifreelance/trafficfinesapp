import { put, select } from 'redux-saga/effects'
import { is } from 'ramda'

// exported to make available for tests
export const selectState = (state) => state

// process STARTUP actions
export function * startup (firebase, action) {

}
