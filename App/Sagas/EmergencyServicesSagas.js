import { put, select, call } from 'redux-saga/effects'
import Actions from '../Redux/EmergencyServicesRedux'

export const selectEmergencyServices = (state) => state.emergencyServices.emergencyServices.asMutable()

export function * searchEmergencyServices (search, { query }) {
  const data = yield select(selectEmergencyServices)
  try {
    const results = yield call(search.search, data, query)
    yield put(Actions.queryChangeResultAction(query, results))
  } catch (e) {
  }
}
