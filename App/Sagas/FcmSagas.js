import { put, select, call, take } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import { is } from 'ramda'
import ProfileActions from '../Redux/ProfileRedux'
import FcmActions from '../Redux/FcmRedux'

export const selectProfile = (state) => state.profile

function progressSagaHelper (func, ...args) {
  // An event channel will let you send an infinite number of events
  // It provides you with an emitter to send these events
  // These events can then be picked up by a saga through a "take" method
  return eventChannel(emitter => {
    const task = func(emitter, ...args)

    // The returned method can be called to cancel the channel
    return () => {
      task.cancel()
    }
  })
}

export function * listenForMessages (firebase, action) {
  console.tron.log('++++++ STARTUP')
  try {
    const channel = yield call(progressSagaHelper, firebase.setOnMessage)
    try {
      // take(END) will cause the saga to terminate by jumping to the finally block
      while (true) {
        // Remember, our helper only emits actions
        // Thus we can directly "put" them
        const message = yield take(channel)
        console.tron.log('++++++ MESSAGE')
        console.tron.log(message)
        yield put(FcmActions.onReceiveMessage(message))
      }
    } catch (error) {
      console.tron.log('++++++ MESSAGE WHILE ERROR')
      console.log(error)
      console.tron.log(error)
      console.tron.error(error)
      console.tron.display(error)
    } finally {
      // Optional
    }
  } catch (error) {
    console.tron.log('++++++ MESSAGE MAIN ERROR')
    console.tron.log(error)
    // return yield put(Actions.updateProfilePictureFailureAction(e.message))
  }
}

export function * getInitialNotification (firebase, action) {
  const message = yield call(firebase.getInitialNotification)
  if (message && message['google.message_id']) {
    yield put(FcmActions.onOpenFromMessage(message))
  }
}

export function * getToken (firebase) {
  try {
    const token = yield call(firebase.getToken)
    return yield put(ProfileActions.updateTokenAction(token))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(ProfileActions.getTokenFailAction(e.message))
  }
}

export function * refreshToken (firebase, action) {
  console.tron.log('++++++ REFRESH TOKEN')
  console.tron.log(action)
  try {
    const channel = yield call(progressSagaHelper, firebase.setOnTokenRefresh)
    try {
      // take(END) will cause the saga to terminate by jumping to the finally block
      while (true) {
        // Remember, our helper only emits actions
        // Thus we can directly "put" them
        const action = yield take(channel)
        console.tron.log('++++ token refreshed')
        console.tron.log(action)
        const { data } = yield select(selectProfile)
        if (data && data.id) {
          console.tron.log('++++++UPDATE TOKEN')
          return yield put(ProfileActions.updateTokenAction(action.token))
        }
      }
    } catch (error) {
    } finally {
      // Optional
    }
  } catch (e) {
    // return yield put(ProfileActions.updateProfilePictureFailureAction(e.message))
  }
}
