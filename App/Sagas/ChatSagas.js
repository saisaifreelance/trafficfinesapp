import { put, select, call, take } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import { populate } from 'react-redux-firebase'
import Actions, { messagesPopulates } from '../Redux/ChatRedux'

export const selectVerificationId = (state) => state.profile.verificationId

export const selectUser = (state) => state.firebase.auth

export const selectProfile = (state) => {
  const { firebase: { profile }, profile: { profile: storeProfile, image } } = state
  return { ...storeProfile, ...profile, image }
}

export const selectChat = ({ chat }) => chat.asMutable()

export const selectFirebase = ({ firebase }) => firebase

export function * sendMessage (api, { chatRoom, message: messageRaw, chat }) {
  // const { _id, text, user: {_id, name, avatar }, createdAt } = messageRaw
  const { _id, text, user, createdAt } = messageRaw
  const message = {
    id: _id,
    text,
    createdAt,
    user: user._id
  }
  yield put(Actions.sendMessageRequestAction(message))

    console.tron.log('##### api.sendMessage')
    console.tron.log(api.sendMessage)
    const response = yield call(api.sendMessage, chatRoom, message, user, chat )
    console.tron.log('##### RESPONSE')
    console.tron.log(response)
    // return yield put(Actions.sendMessageSuccessAction(message, response))

}

export function * fetchStart ({ type, path }) {
  // Meta
  let parts = path.match(/chatRooms\/(.*)\/meta/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]
    return yield put(Actions.fetchMetaStartAction(chatRoom))
  }

  // Messages
  parts = path.match(/chatRooms\/(.*)\/messages/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]
    return yield put(Actions.fetchMessagesStartAction(chatRoom))
  }
}

export function *  fetchSet ({ type, path, data, ordered }) {
  const state = yield select(selectChat)
  const firebase = yield select(selectFirebase)

  // Meta
  let parts = path.match(/chatRooms\/(.*)\/meta/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]
    return yield put(Actions.fetchMetaSuccessAction(chatRoom, data))
  }
  // Messages
  parts = path.match(/chatRooms\/(.*)\/messages/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]

    if (!Array.isArray(ordered)) {
      if (!state.firstId) {
        return yield put(Actions.fetchMessagesSuccessAction(chatRoom, null, [], false))
      }
      return
    }

    const populated = populate(firebase, `chatRooms/${chatRoom}/messages`, messagesPopulates)
    const messages = ordered.map(({key, value}) => {
      const message = {
        _id: populated[key].id,
        text: populated[key].text,
        createdAt: populated[key].createdAt,
        user: {}
      }

      if (populated[key].user && typeof populated[key].user === 'object') {
        message.user = {
          _id: populated[key].user.id,
          name: populated[key].user.name,
          avatar: populated[key].user.downloadUrl
        }
      }
      return message
    })

    if (!state.firstId) {
      return yield put(Actions.fetchMessagesSuccessAction(chatRoom, messages[messages.length - 1]._id, messages.reverse(), false))
    }

    if (state.firstId === messages[0]._id) {
      return yield put(Actions.fetchMessagesSuccessAction(chatRoom, messages[messages.length - 1]._id, messages.reverse(), true))
    }

    if (state.firstId === messages[messages.length - 1]._id) {
      return yield put(Actions.fetchMessagesSuccessAction(chatRoom, messages[messages.length - 1]._id, messages.reverse(), false))
    }

  }
}

export function *  fetchError ({ type, path, error }) {
  let parts = path.match(/chatRooms\/(.*)\/meta/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]
    return yield put(Actions.fetchMetaFailAction(chatRoom, error))
  }

  // Messages
  parts = path.match(/chatRooms\/(.*)\/messages/)
  if (parts && parts.length === 2) {
    const chatRoom = parts[1]
    return yield put(Actions.fetchMessagesFailAction(chatRoom, error))
  }
}

function progressSagaHelper (func, ...args) {
  // An event channel will let you send an infinite number of events
  // It provides you with an emitter to send these events
  // These events can then be picked up by a saga through a "take" method
  return eventChannel(emitter => {
    const task = func(emitter, ...args)

    // The returned method can be called to cancel the channel
    return () => {
      task.cancel()
    }
  })
}
