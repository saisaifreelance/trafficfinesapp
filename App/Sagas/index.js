import { takeLatest } from 'redux-saga/effects'
import { actionTypes as ReactReduxFirebaseTypes } from 'react-redux-firebase'
import API from '../Services/Api'
import Firebase from '../Services/Firebase'
import * as Facebook from '../Services/Facebook'
import PushNotifications from '../Services/PushNotifications'
import Search from '../Services/Search'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { FacebookTypes } from '../Redux/FacebookRedux'
import { ProfileTypes } from '../Redux/ProfileRedux'
import { TrafficFinesTypes } from '../Redux/TrafficFinesRedux'
import { EmergencyServicesTypes } from '../Redux/EmergencyServicesRedux'
import { ChatTypes } from '../Redux/ChatRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { appInvite } from './FacebookSagas'
import { login, logout, verify, signInAnonymously, setProfile, updateProfile, updateProfilePicture, updateToken, logoutRedirect } from './ProfileSagas'
import { searchTrafficFines } from './TrafficFinesSagas'
import { searchEmergencyServices } from './EmergencyServicesSagas'
import { sendMessage, fetchStart, fetchSet, fetchError } from './ChatSagas'
import { listenForMessages, getInitialNotification, refreshToken, getToken, onMessage } from './FcmSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()
const firebase = Firebase.create()
const pushNotifications = PushNotifications.create()
const search = Search.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield [
    // takeLatest(ReactReduxFirebaseTypes.LOGOUT, logoutRedirect),
    takeLatest(StartupTypes.STARTUP, startup, firebase),
    takeLatest(StartupTypes.STARTUP, listenForMessages, firebase),
    takeLatest(StartupTypes.STARTUP, getInitialNotification, firebase),
    takeLatest(FacebookTypes.APP_INVITE, appInvite, Facebook.appInvite),
    takeLatest(ProfileTypes.LOGIN_REQUEST_ACTION, login, firebase),
    takeLatest(ProfileTypes.LOGOUT_REQUEST_ACTION, logout, firebase),
    takeLatest(ProfileTypes.VERIFY_REQUEST_ACTION, verify, firebase),
    takeLatest(ProfileTypes.SIGN_IN_ANONYMOUSLY_ACTION, signInAnonymously, firebase),
    takeLatest(ProfileTypes.UPDATE_PROFILE_REQUEST_ACTION, updateProfile, firebase),
    takeLatest(ProfileTypes.UPDATE_PROFILE_PICTURE_ACTION, updateProfilePicture, firebase),
    takeLatest(ProfileTypes.UPDATE_TOKEN_ACTION, updateToken, firebase),
    takeLatest(ReactReduxFirebaseTypes.SET_PROFILE, setProfile, firebase),
    takeLatest(ProfileTypes.REFRESH_PROFILE_ACTION, refreshToken, firebase),
    takeLatest(ProfileTypes.REFRESH_PROFILE_ACTION, getToken, firebase),
    takeLatest(TrafficFinesTypes.QUERY_CHANGE_ACTION, searchTrafficFines, search),
    takeLatest(EmergencyServicesTypes.QUERY_CHANGE_ACTION, searchEmergencyServices, search),
    takeLatest(ChatTypes.SEND_MESSAGE_ACTION, sendMessage, api),
    takeLatest(ReactReduxFirebaseTypes.START, fetchStart),
    takeLatest(ReactReduxFirebaseTypes.SET, fetchSet),
    takeLatest(ReactReduxFirebaseTypes.ERROR, fetchError),
  ]
}
