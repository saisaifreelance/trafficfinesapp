export default {
  enableRedirectHandling: false, // required,
  userProfile: 'drivers',
  // presence: 'presence',
  // sessions: 'sessions',
  profileParamsToPopulate: [
    { child: 'profile_pic', root: '/images/profile_pics' }
  ],
  autoPopulateProfile: false,
  setProfilePopulateResults: true
}
