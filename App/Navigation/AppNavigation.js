import React from 'react'
import { StackNavigator } from 'react-navigation'
import List from '../Containers/List'
import DonutScreen from '../Containers/DonutScreen'
import DrawerContainer from '../Containers/DrawerContainer'
import ExpandableListScreen from '../Containers/ExpandableListScreen'
import EmergencyServiceScreen from '../Containers/EmergencyServiceScreen'
import TrafficFineScreen from '../Containers/TrafficFineScreen'
import EmergencyServicesScreen from '../Containers/EmergencyServicesScreen'
import TrafficFinesScreen from '../Containers/TrafficFinesScreen'
import ProfileScreen from '../Containers/ProfileScreen'
import AuthScreen from '../Containers/AuthScreen'
import HomeScreen from '../Components/Home'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  List: { screen: List },
  DrawerContainer: { screen: DrawerContainer },
  ExpandableListScreen: { screen: ExpandableListScreen },
  EmergencyServiceScreen: { screen: (props) => (<EmergencyServiceScreen {...props} {...props.navigation.state.params} />) },
  TrafficFineScreen: { screen: (props) => (<TrafficFineScreen {...props} {...props.navigation.state.params}/>) },
  EmergencyServicesScreen: { screen: EmergencyServicesScreen },
  TrafficFinesScreen: { screen: TrafficFinesScreen },
  ProfileScreen: { screen: ProfileScreen },
  AuthScreen: { screen: AuthScreen },
  HomeScreen: { screen: HomeScreen },
  LaunchScreen: { screen: LaunchScreen },
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
