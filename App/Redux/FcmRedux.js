import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import { NavigationActions } from 'react-navigation'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  onReceiveMessage: ['message'],
  onOpenFromMessage: ['message'],
  clearMessage: null,
  clearMessages: null,
})

export const FcmTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  messages: [],
  initialMessage: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const onReceiveMessage = (state, { message: messageRaw }) => {
  switch (messageRaw.type) {
    case 'TRAFFIC_FINE_CHAT_MESSAGE': {
      const { userId, userDownloadUrl, userName, messageId, messageText, chatRoomId, chatRoomTitle, chatRoomDescription, fcm: { title, body }} = messageRaw
      const message = {
        id: messageId,
        image: userDownloadUrl,
        title: `${userName} in ${chatRoomTitle}`,
        description: messageText,
        action: {
          type: NavigationActions.NAVIGATE,
          routeName: 'TrafficFineScreen',
          params: {
            id: chatRoomId
          }
        }
      }
      const messages = state.messages.asMutable()
      messages.push(message)
      return state.merge({messages})
    }
    default:
      return state
    break
  }
}

export const onOpenFromMessage = (state, { message: messageRaw }) => {
  switch (messageRaw.type) {
    case 'TRAFFIC_FINE_CHAT_MESSAGE': {
      const { userId, userDownloadUrl, userName, messageId, messageText, chatRoomId, chatRoomTitle, chatRoomDescription, fcm: { title, body }} = messageRaw
      const initialMessage = {
        id: messageId,
        image: userDownloadUrl,
        title: `${userName} in ${chatRoomTitle}`,
        description: messageText,
        action: {
          type: NavigationActions.NAVIGATE,
          routeName: 'TrafficFineScreen',
          params: {
            id: chatRoomId
          }
        }
      }
      return state.merge({ initialMessage })
    }
    default:
      return state
    break
  }
}

export const onClearInitialMessage = (state) => {
  return state.merge({ initialMessage: null })
}

export const onClearMessage = (state) => {
  const messages = state.messages.asMutable().slice(1)
  return state.merge({messages})
}

export const onClearMessages = (state) => {
  const initialMessage = state.initialMessage
  state.merge({messages: []})
}

export const onNavigate = (state, action) => {
  const { routeName, params } = action
  if (routeName === 'TrafficFineScreen') {
    id = params && typeof params === 'object' && params.idea
    const messages = state.messages.asMutable().filter((message) => {
      return message.action.params.id !== id
    })
    let initialMessage = state.initialMessage
    if (initialMessage) {
      initialMessage = initialMessage.asMutable()
      if (initialMessage.action && initialMessage.action.params && initialMessage.action.params.id === id) {
        initialMessage = null
      }
    }
    return state.merge({ messages, initialMessage })
  }
  return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_RECEIVE_MESSAGE]: onReceiveMessage,
  [Types.ON_OPEN_FROM_MESSAGE]: onOpenFromMessage,
  [Types.CLEAR_MESSAGES]: onClearMessages,
  [Types.CLEAR_MESSAGE]: onClearMessage,
  [NavigationActions.NAVIGATE]: onNavigate
})

const selectFcmDomain = (state) => {
  return state.fcm.asMutable()
}

export const makeSelectMessage = () => createSelector(
  selectFcmDomain,
  (fcm) => {
    // const { messages } = fcm
    return fcm.messages[0]
  }
)

export const makeSelectInitialMessage = () => createSelector(
  selectFcmDomain,
  (fcm) => {
    // const { messages } = fcm
    return fcm.initialMessage
  }
)
