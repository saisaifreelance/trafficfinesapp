import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import { REHYDRATE } from 'redux-persist/constants'
import { actionTypes } from 'react-redux-firebase'

import defaultTrafficFines from '../Data/TrafficFines'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  queryChangeAction: ['query'],
  queryChangeResultAction: ['query', 'results'],
  createChatRoomAction: ['chatRoom', 'id', 'text'],
  createChatRoomRequestAction: ['message', 'path'],
  createChatRoomSuccessAction: ['message', 'path'],
  createChatRoomFailAction: ['message', 'path', 'error'],
}, {prefix: 'TrafficFinesRedux/'})

export const TrafficFinesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  isFetching: null,
  isSearching: null,
  trafficFines: null,
  metaTrafficFines: null,
  trafficFinesCategories: null,
  metaTrafficFinesCategories: null,
  hydrated: false,
  trafficFinesSet: false,
  metaTrafficFinesSet: false,
  trafficFinesCategoriesSet: false,
  metaTrafficFinesCategoriesSet: false,
  searchResults: null,
  query: '',
})

/* ------------- Reducers ------------- */

export const rehydrate = (state, { payload: { trafficFines } }) => {
  let returnState = state.set('hydrated', true)
  if (trafficFines) {
    if (!state.trafficFinesSet) {
      if (trafficFines.trafficFines) {
        returnState = returnState.set('trafficFines', trafficFines.trafficFines)
      } else {
        returnState = returnState.set('trafficFines', defaultTrafficFines.traffic_fines)
      }
    }
    if (!state.metaTrafficFinesSet) {
      if (trafficFines.metaTrafficFines) {
        returnState = returnState.set('metaTrafficFines', trafficFines.metaTrafficFines)
      } else {
        returnState = returnState.set('metaTrafficFines', defaultTrafficFines.meta_traffic_fines)
      }
    }
    if (!state.trafficFinesCategoriesSet) {
      if (trafficFines.trafficFinesCategories && !state.trafficFinesCategoriesSet) {
        returnState = returnState.set('trafficFinesCategories', trafficFines.trafficFinesCategories)
      } else {
        returnState = returnState.set('trafficFinesCategories', defaultTrafficFines.traffic_fines_categories)
      }
    }
    if (!state.metaTrafficFinesCategories) {
      if (trafficFines.metaTrafficFinesCategories) {
        returnState = returnState.set('metaTrafficFinesCategories', trafficFines.metaTrafficFinesCategories)
      } else {
        returnState = returnState.set('metaTrafficFinesCategories', defaultTrafficFines.meta_traffic_fines_categories)
      }
    }
  } else {
    returnState = returnState
      .set('trafficFines', defaultTrafficFines.traffic_fines)
      .set('metaTrafficFines', defaultTrafficFines.meta_traffic_fines)
      .set('trafficFinesCategories', defaultTrafficFines.traffic_fines_categories)
      .set('metaTrafficFinesCategories', defaultTrafficFines.meta_traffic_fines_categories)
  }
  return returnState
}

export const setFirebaseData = (state, action) => {
  switch (action.path) {
    case '/traffic_fines':
      return state.merge({
        trafficFines: action.data,
        trafficFinesSet: true
      })
    case '/meta_traffic_fines':
      return state.merge({
        metaTrafficFines: action.data,
        metaTrafficFinesSet: true
      })
    case '/traffic_fines_categories':
      return state.merge({
        trafficFinesCategories: action.data,
        trafficFinesCategoriesSet: true
      })
    case '/meta_traffic_fines_categories':
      return state.merge({
        metaTrafficFinesCategories: action.data,
        metaTrafficFinesCategoriesSet: true
      })
    default:
      return state
  }
}

export const setQuery = (state, { query }) => {
  return state
    .set('query', query)
    .set('isSearching', `Searching for '${query}'...`)
}

export const setQueryResult = (state, { query, results }) => {
  return state
    .set('query', query)
    .set('searchResults', results)
    .set('isSearching', null)
}

export const createChatRoomRequest = (state, { chatRoom }) => {
  return state.set('isFetching', 'initialising chat')
}

export const createChatRoomSuccess = (state, { chatRoom }) => {
  return state.set('isFetching', null)
}

export const createChatRoomFail = (state, { error }) => {
  return state.merge({'isFetching': null, error})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.QUERY_CHANGE_ACTION]: setQuery,
  [Types.QUERY_CHANGE_RESULT_ACTION]: setQueryResult,
  [Types.CREATE_CHAT_ROOM_REQUEST_ACTION]: createChatRoomRequest,
  [Types.CREATE_CHAT_ROOM_SUCCESS_ACTION]: createChatRoomSuccess,
  [Types.CREATE_CHAT_ROOM_FAIL_ACTION]: createChatRoomFail,
  [REHYDRATE]: rehydrate,
  [actionTypes.SET]: setFirebaseData
})

/* ------------- Selectors ------------- */

const selectTrafficFinesDomain = ({ trafficFines }) => trafficFines.asMutable()

export const makeSelectTrafficFine = (id) => createSelector(
  selectTrafficFinesDomain,
  (trafficFinesDomain) => {
    let {
      isFetching,
      trafficFines,
      metaTrafficFines,
      hydrated,
      trafficFinesSet,
      metaTrafficFinesSet
    } = trafficFinesDomain
    return {
      isFetching,
      hydrated,
      trafficFine: trafficFines ? trafficFines[id] : {},
      metaTrafficFines,
      trafficFinesSet,
      metaTrafficFinesSet
    }
  })

export const makeSelectTrafficFines = () => createSelector(
  selectTrafficFinesDomain,
  (trafficFinesDomain) => {
    return trafficFinesDomain
    let {
      isFetching,
      isSearching,
      trafficFines,
      metaTrafficFines,
      trafficFinesCategories,
      metaTrafficFinesCategories,
      hydrated,
      trafficFinesSet,
      metaTrafficFinesSet,
      trafficFinesCategoriesSet,
      metaTrafficFinesCategoriesSet,
      query,
      searchResults
    } = trafficFinesDomain
    return {
      isFetching,
      isSearching,
      trafficFines,
      metaTrafficFines,
      trafficFinesCategories,
      metaTrafficFinesCategories,
      hydrated,
      trafficFinesSet,
      metaTrafficFinesSet,
      trafficFinesCategoriesSet,
      metaTrafficFinesCategoriesSet,
      query,
      searchResults
    }
  }
)
