import Actions from './ProfileRedux'
export default (prevState, nextState, action, dispatch) => {
  let id = null
  try {
    id = nextState.profile.data.id
  } catch (e) {
    return
  }
  if (!id) {
    return
  }
  let oldId = null
  try {
    oldId = prevState.profile.data.id
  } catch (e) {
    return dispatch(Actions.refreshProfileAction(id))
  }
  if (id !== oldId) {
    dispatch(Actions.refreshProfileAction(id))
  }
}
