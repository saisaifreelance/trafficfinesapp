import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: require('./NavigationRedux').reducer,
    profile: require('./ProfileRedux').reducer,
    trafficFines: require('./TrafficFinesRedux').reducer,
    emergencyServices: require('./EmergencyServicesRedux').reducer,
    chat: require('./ChatRedux').reducer,
    fcm: require('./FcmRedux').reducer,
    firebase: require('react-redux-firebase').firebaseStateReducer
  })

  return configureStore(rootReducer, rootSaga)
}
