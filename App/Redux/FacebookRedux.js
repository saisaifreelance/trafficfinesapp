import { createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  appInvite: null
})

export const FacebookTypes = Types
export default Creators
