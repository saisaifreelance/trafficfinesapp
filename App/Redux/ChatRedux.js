import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import { populate, actionTypes } from 'react-redux-firebase'
import ReactReduxFirebaseConfig from '../Config/ReactReduxFirebaseConfig'

const pageSize = 10

export const messagesPopulates = [
  { child: 'user', root: 'drivers' }
  // { child: 'user/profile_pic', root: 'images/profile_pics' },
]

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  messageChangeAction: ['text'],
  messageChangeRequestAction: ['text'],
  sendMessageAction: ['chatRoom', 'message', 'chat'],
  sendMessageRequestAction: ['message', 'path'],
  sendMessageSuccessAction: ['message', 'path'],
  sendMessageFailAction: ['message', 'path', 'error'],
  fetchMessagesStartAction: ['chatRoom'],
  fetchMessagesSuccessAction: ['chatRoom', 'firstId', 'messages', 'isNew'],
  fetchMessagesFailAction: ['chatRoom', 'error'],
  fetchMetaStartAction: ['chatRoom'],
  fetchMetaSuccessAction: ['chatRoom', 'meta'],
  fetchMetaFailAction: ['chatRoom', 'error'],
  loadEarlierMessagesAction: null
}, {prefix: 'ChatsRedux/'})

export const ChatTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  isFetching: null,
  count: 10,
  firstId: null,
  message: {
    text: ''
  },
  chats: {},
})

/* ------------- Reducers ------------- */

export const messageChange = (state, { text }) => {
  return state.setIn(['message', 'text'], text)
}

export const sendMessageRequest = (state, { message }) => {
  return state.set('message', { ...message, isFetching: 'sending message...' })
}

export const sendMessageSuccess = (state, { message }) => {
  return state.set('message', message)
}

export const sendMessageFail = (state, { message, error }) => {
  return state.set('message', { ...message, error })
}

export const fetchMessagesStart = (state, { chatRoom }) => {
  return state;
}

export const fetchMessagesSuccess = (state, action) => {
  const { chatRoom, firstId, messages, isNew } = action
  if (!isNew) {
    return state
      .set('firstId', firstId)
      .setIn(['chats', chatRoom, 'messages', 'ordered'], messages)
      .setIn(['chats', chatRoom, 'messages', 'isFetching', null])
  }

  return state
    .set('firstId', firstId)
    .setIn(['chats', chatRoom, 'messages', 'new'], messages)
}

export const fetchMessagesFail = (state, { chatRoom, error }) => {
  return state
}


export const fetchMetaStart = (state, { chatRoom }) => {
  return state;
}

export const fetchMetaSuccess = (state, action) => {
  const { chatRoom, data } = action
  return state
    .setIn(['chats', chatRoom, 'meta', 'data'], data)
    .setIn(['chats', chatRoom, 'meta', 'isFetching', null])
}

export const fetchMetaFail = (state, { chatRoom, error }) => {
  return state
}
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MESSAGE_CHANGE_REQUEST_ACTION]: messageChange,
  [Types.SEND_MESSAGE_REQUEST_ACTION]: sendMessageRequest,
  [Types.SEND_MESSAGE_SUCCESS_ACTION]: sendMessageSuccess,
  [Types.SEND_MESSAGE_FAIL_ACTION]: sendMessageFail,
  [Types.FETCH_MESSAGES_START_ACTION]: fetchMessagesStart,
  [Types.FETCH_MESSAGES_SUCCESS_ACTION]: fetchMessagesSuccess,
  [Types.FETCH_MESSAGES_FAIL_ACTION]: fetchMessagesFail,
  [Types.FETCH_META_START_ACTION]: fetchMetaStart,
  [Types.FETCH_META_SUCCESS_ACTION]: fetchMetaSuccess,
  [Types.FETCH_META_FAIL_ACTION]: fetchMetaFail,
  // [actionTypes.START]: fetchStart,
  // [actionTypes.SET]: fetchSet,
  // [actionTypes.ERROR]: fetchError,
})

/* ------------- Selectors ------------- */

const selectChatDomain = ({ chat }) => chat.asMutable()

const selectFirebase = (state) => {
  return state.firebase
}

export const makeSelectChatRoomMeta = (chatRoom) => createSelector(
  selectChatDomain,
  (chatDomain) => {
    try {
      return chat.chats[chatRoom].meta
    } catch (e) {
      return null
    }
  }
)

export const makeSelectChatRoomMessages = (chatRoom) => createSelector(
  selectChatDomain,
  (chat) => {
    try {
      return chat.chats[chatRoom].messages.ordered
    } catch (e) {
      return null
    }
  }
)

export const makeSelectChatRoomMessagesOG = (chatRoom) => ({ firebase }) => {
  const { ordered } = firebase
  const populated = populate(firebase, `chatRooms/${chatRoom}/messages`, messagesPopulates)

  try {
    return ordered.chatRooms[chatRoom].messages.map((chatRoom) => {
      const message = populated[chatRoom.key]
      return {
        _id: message.id,
        text: message.text,
        createdAt: new Date(message.createdAt),
        user: {
          _id: message.user.id,
          name: message.user.name,
          avatar: message.user.downloadUrl
        },
      }
    }).reverse()
  } catch (e) {
    return []
  }
}


export const makeSelectProfile = () => ({ firebase }) => {
  try {
    const profile = populate(firebase, 'profile', ReactReduxFirebaseConfig.profileParamsToPopulate, {su: 'cks'})
    if (typeof profile.profile_pic === 'string') {
      profile.profile_pic = firebase.data.images.profile_pics[profile.profile_pic]
    }
    return profile
  } catch (e) {
    return null
  }
}
export const makeSelectChat = (chatRoom) => createSelector(
  selectChatDomain,
  (chatDomain) => chatDomain)
