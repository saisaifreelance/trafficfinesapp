import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import { REHYDRATE } from 'redux-persist/constants'
import { actionTypes } from 'react-redux-firebase'

import defaultEmergencyServices from '../Data/EmergencyServices'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  queryChangeAction: ['query'],
  queryChangeResultAction: ['query', 'results']
}, {prefix: 'EmergencyServicesRedux/'})

export const EmergencyServicesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  isFetching: null,
  isSearching: null,
  emergencyServices: null,
  metaEmergencyServices: null,
  emergencyServicesCategories: null,
  metaEmergencyServicesCategories: null,
  hydrated: false,
  emergencyServicesSet: false,
  metaEmergencyServicesSet: false,
  emergencyServicesCategoriesSet: false,
  metaEmergencyServicesCategoriesSet: false,
  query: '',
  searchResults: null
})

/* ------------- Reducers ------------- */

export const rehydrate = (state, { payload: { emergencyServices } }) => {
  let returnState = state.set('hydrated', true)
  if (emergencyServices) {
    if (!state.emergencyServicesSet) {
      if (emergencyServices.emergencyServices) {
        returnState = returnState.set('emergencyServices', emergencyServices.emergencyServices)
      } else {
        returnState = returnState.set('emergencyServices', defaultEmergencyServices.emergency_services)
      }
    }
    if (!state.metaEmergencyServicesSet) {
      if (emergencyServices.metaEmergencyServices) {
        returnState = returnState.set('metaEmergencyServices', emergencyServices.metaEmergencyServices)
      } else {
        returnState = returnState.set('metaEmergencyServices', defaultEmergencyServices.meta_emergency_services)
      }
    }
    if (!state.emergencyServicesCategoriesSet) {
      if (emergencyServices.emergencyServicesCategories && !state.emergencyServicesCategoriesSet) {
        returnState = returnState.set('emergencyServicesCategories', emergencyServices.emergencyServicesCategories)
      } else {
        returnState = returnState.set('emergencyServicesCategories', defaultEmergencyServices.emergency_services_categories)
      }
    }
    if (!state.metaEmergencyServicesCategories) {
      if (emergencyServices.metaEmergencyServicesCategories) {
        returnState = returnState.set('metaEmergencyServicesCategories', emergencyServices.metaEmergencyServicesCategories)
      } else {
        returnState = returnState.set('metaEmergencyServicesCategories', defaultEmergencyServices.meta_emergency_services_categories)
      }
    }
  }
  return returnState
}

export const setFirebaseData = (state, action) => {
  switch (action.path) {
    case '/emergency_services':
      return state.merge({
        emergencyServices: action.data,
        emergencyServicesSet: true
      })
    case '/meta_emergency_services':
      return state.merge({
        metaEmergencyServices: action.data,
        metaEmergencyServicesSet: true
      })
    case '/emergency_services_categories':
      return state.merge({
        emergencyServicesCategories: action.data,
        emergencyServicesCategoriesSet: true
      })
    case '/meta_emergency_services_categories':
      return state.merge({
        metaEmergencyServicesCategories: action.data,
        metaEmergencyServicesCategoriesSet: true
      })
    default:
      return state
  }
}

export const setQuery = (state, { query }) => {
  return state
    .set('query', query)
    .set('isSearching', `Searching for '${query}'...`)
}

export const setQueryResult = (state, { query, results }) => {
  return state
    .set('query', query)
    .set('searchResults', results)
    .set('isSearching', null)
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.QUERY_CHANGE_ACTION]: setQuery,
  [Types.QUERY_CHANGE_RESULT_ACTION]: setQueryResult,
  [REHYDRATE]: rehydrate,
  [actionTypes.SET]: setFirebaseData
})

/* ------------- Selectors ------------- */

const selectEmergencyServicesDomain = ({ emergencyServices }) => emergencyServices.asMutable()

export const makeSelectEmergencyService = (id) => createSelector(
  selectEmergencyServicesDomain,
  (emergencyServicesDomain) => {
    let {
      isFetching,
      emergencyServices,
      metaEmergencyServices,
      hydrated,
      emergencyServicesSet,
      metaEmergencyServicesSet,
      query,
      searchResults
    } = emergencyServicesDomain
    return {
      isFetching,
      emergencyServices,
      metaEmergencyServices,
      hydrated,
      emergencyServicesSet,
      metaEmergencyServicesSet,
      query,
      searchResults,
      emergencyService: emergencyServices && id ? emergencyServices[id] : null
    }
  }
)

export const makeSelectEmergencyServices = () => createSelector(
  selectEmergencyServicesDomain,
  (emergencyServicesDomain) => {
    let {
      isFetching,
      emergencyServices,
      metaEmergencyServices,
      emergencyServicesCategories,
      metaEmergencyServicesCategories,
      hydrated,
      emergencyServicesSet,
      metaEmergencyServicesSet,
      emergencyServicesCategoriesSet,
      metaEmergencyServicesCategoriesSet,
      query,
      searchResults
    } = emergencyServicesDomain
    return {
      isFetching,
      emergencyServices,
      metaEmergencyServices,
      emergencyServicesCategories,
      metaEmergencyServicesCategories,
      hydrated,
      emergencyServicesSet,
      metaEmergencyServicesSet,
      emergencyServicesCategoriesSet,
      metaEmergencyServicesCategoriesSet,
      query,
      searchResults,
    }
  }
)
