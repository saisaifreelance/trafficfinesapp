import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TextInput } from 'react-native'
import styles from './Styles/TextFieldStyle'
import { Colors } from '../Themes'

export default class TextField extends Component {
  // // Prop type warnings
  static propTypes = {
    value: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    onSubmitEditing: PropTypes.func.isRequired,
    textInputProps: PropTypes.object.isRequired
  }

  // // Defaults for props
  static defaultProps = {
    textInputProps: {
      blurOnSubmit: true,
      enablesReturnKeyAutomatically: true,
      underlineColorAndroid: 'rgba(0,0,0,0)'
    }
  }

  render () {
    const error = this.props.error ? (<Text style={styles.errorText}>{this.props.error}</Text>) : null
    return (
      <View>
        <View style={styles.formField}>
          <TextInput
            style={[styles.textInput, {
              margin: 0,
              padding: 0,
              borderBottomColor: 'red'
            }]}
            autoCorrect={false}
            value={this.props.value}
            placeholder={this.props.label}
            placeholderTextColor={Colors.steel}
            onChangeText={this.props.onChangeText}
            blurOnSubmit
            enablesReturnKeyAutomatically
            onSubmitEditing={this.props.onSubmitEditing}
            {...this.props.textInputProps}
            underlineColorAndroid='rgba(0,0,0,0)'
          />
        </View>
        {error}
      </View>
    )
  }
}
