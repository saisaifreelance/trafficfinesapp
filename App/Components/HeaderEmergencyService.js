import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/HeaderTrafficFineStyle'
import { Colors } from '../Themes'
import { EmergencyService } from '../Types'
import MarkdownText from './MarkdownText'

export default class HeaderEmergencyService extends Component {
  // Prop type warnings
  static propTypes = {
    emergencyService: EmergencyService.isRequired,
    selected: PropTypes.bool,
    hovered: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // Defaults for props
  static defaultProps = {
    selected: false,
    hovered: false
  }

  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress()
  }

  handleLongPress () {
    this.props.onLongPress()
  }

  render () {
    const props = {
      style: [styles.container],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    return (
      <TouchableOpacity {...props}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.emergencyService.title}</Text>
        </View>
        <MarkdownText>{this.props.emergencyService.text}</MarkdownText>
      </TouchableOpacity>
    )
  }
}
