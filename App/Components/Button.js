import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image, Text, TouchableOpacity, TouchableNativeFeedback, Platform } from 'react-native'
import styles from './Styles/ButtonStyle'
import { Colors, Metrics } from '../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Button extends Component {
  // // Prop type warnings
  static propTypes = {
    children: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
    icon: PropTypes.number,
    iconFont: PropTypes.string.isRequired,
    before: PropTypes.element,
    after: PropTypes.element,
    more: PropTypes.bool,
    backgroundColor: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    textStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    multiline: PropTypes.bool.isRequired,
  }
  //
  // Defaults for props
  static defaultProps = {
    color: Colors.snow,
    backgroundColor: Colors.amber,
    iconFont: 'EvilIcons',
    more: false,
    multiline: true
  }

  // constructor (props) {
  //   super(props)
  // }

  render () {
    const content = []
    let text = this.props.children
    if (text && typeof text === 'string') {
      const parts = text.split(' ')
      text = ''
      for (let i = 0; i < parts.length; i++) {
        text += parts[i].charAt(0).toUpperCase() + parts[i].slice(1).toLowerCase() + ' '
      }
      text = text.trim()
    }
    if (this.props.icon) {
      content.push(<Image
        key='icon'
        source={this.props.icon}
        resizeMode='contain'
        style={styles.buttonIcon}
      />)
      content.push(<Text
        key='text'
        style={[styles.buttonTextIcon, {color: this.props.color}, this.props.textStyle]}
        numberOfLines={this.props.multiline ? null : 1}>{text}</Text>)
    }

    if (!this.props.icon && this.props.more) {
      content.push(<Text key='text' style={[styles.buttonTextIcon, {color: this.props.color}, this.props.textStyle]} numberOfLines={this.props.multiline ? null : 1}>{text}</Text>)
    }

    if (this.props.more) {
      content.push(<Icon key='more' name='keyboard-arrow-right' size={Metrics.icons.medium} color={Colors.snow} />)
    }

    if (!this.props.icon && !this.props.more) {
      content.push(<Text key='text' style={[styles.buttonText, {color: this.props.color}, this.props.textStyle]} numberOfLines={this.props.multiline ? null : 1}>{text}</Text>)
    }

    if (Platform.OS !== 'ios') {
      return (
        <TouchableNativeFeedback
          onPress={this.props.onPress}
          background={TouchableNativeFeedback.Ripple(Colors.amber)}>
          <View
            onLayout={this.onLayout}
            style={[styles.button, this.props.style, {backgroundColor: this.props.backgroundColor}]}
            onPress={this.props.onPress}>
            {this.props.before}
            {content}
            {this.props.after}
          </View>
        </TouchableNativeFeedback>
      )
    }

    return (
      <TouchableOpacity
        onLayout={this.onLayout}
        style={[styles.button, this.props.style, {backgroundColor: this.props.backgroundColor}]}
        onPress={this.props.onPress}>
        {this.props.before}
        {content}
        {this.props.after}
      </TouchableOpacity>
    )
  }
}
