import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text } from 'react-native'
import styles from './Styles/ListItemTrafficFineCategoryStyle'
import { Colors } from '../Themes'
import MarkdownText from './MarkdownText'
import Badge from './Badge'
import { TrafficFineCategory } from '../Types'

export default class ListItemTrafficFineCategory extends Component {
  // Prop type warnings
  static propTypes = {
    category: TrafficFineCategory.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.category.id)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex)
  }

  render () {
    const { category } = this.props
    const cardStyles = {}
    let hoverContent = null
    let faceContent = null
    let selectedContent = null
    let title = null
    let description = null
    let divider = null
    let text = null
    let badge = null
    let props = {
      style: [styles.container, this.props.style, cardStyles],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    if (category.title) {
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{category.title}</Text>
        </View>
      )
    }

    if (category.unread) {
      badge = (<Badge style={{position: 'absolute', right: 0, top: 0, elevation: 2}} >{category.unread}</Badge>)
    }

    if (category.traffic_fines && typeof category.traffic_fines === 'object') {
      hoverContent = (
        <View style={styles.hoverContent}>
          <Text>{Object.keys(category.traffic_fines).length} Fines</Text>
        </View>
      )
    }

    if (this.props.selected === this.props.cardIndex) {
      props.disabled = true
      if (category.description) {
        description = (
          <View style={styles.fineContainer}>
            <Text style={styles.fine}>{category.description}</Text>
          </View>
        )
      }

      if (category.text) {
        text = (<MarkdownText>{category.text}</MarkdownText>)
      }
      selectedContent = (<View style={styles.selectedContent} />)
      faceContent = null
      badge = null
    }

    else if (this.props.hovered === this.props.cardIndex) {
      if (category.description) {
        description = (
          <View style={styles.fineContainer}>
            <Text style={styles.fine}>{category.description}</Text>
          </View>
        )
      }
      const categoryText = `${category.text.slice(0, 160)}...`
      if (category.text) {
        text = (<MarkdownText>{categoryText}</MarkdownText>)
      }
    }

    if ((this.props.selected !== this.props.cardIndex) && typeof this.props.selected === 'number') {
      cardStyles.height = 0
      cardStyles.overflow = 'hidden'
      hoverContent = null
      faceContent = null
      selectedContent = null
      title = null
      text = null
      props = {
        disabled: true
      }
    }

    if (description || text || hoverContent) {
      divider = <View style={styles.divider} />
    }

    return (
      <View>
        <TouchableOpacity {...props}>
          {title}
          {divider}
          {description}
          {text}
          {hoverContent}
          {faceContent}
        </TouchableOpacity>
        {selectedContent}
        {badge}
      </View>
    )
  }
}
