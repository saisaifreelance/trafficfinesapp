import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/HeaderTrafficFineStyle'
import { Colors } from '../Themes'
import { TrafficFine } from '../Types'
import MarkdownText from './MarkdownText'
import Badge from './Badge'

const MAX_TITLE_TEXT_CHARACTERS = 60

export default class HeaderTrafficFine extends Component {
  // Prop type warnings
  static propTypes = {
    trafficFine: TrafficFine.isRequired,
    selected: PropTypes.bool,
    hovered: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // Defaults for props
  static defaultProps = {
    selected: false,
    hovered: false
  }

  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress()
  }

  handleLongPress () {
    this.props.onLongPress()
  }

  render () {
    const props = {
      style: [styles.container],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }
    let title = null
    let text = null

    if (!this.props.hovered && !this.props.selected) {
      title = (
        <View style={styles.titleContainerDefault}>
          <Text numberOfLines={1} style={styles.titleDefault}>{this.props.trafficFine.title}</Text>
          <Text style={styles.fine}>{this.props.trafficFine.description}</Text>
        </View>
      )
      text = null
    }

    if (this.props.hovered) {
      title = (
        <View style={styles.titleContainerDefault}>
          <Text style={styles.title}>{this.props.trafficFine.title}</Text>
          <Text style={styles.fine}>{this.props.trafficFine.description}</Text>
        </View>
      )
      text = null
    }
    if (this.props.selected) {
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.trafficFine.title}</Text>
          <Text style={styles.fine}>{this.props.trafficFine.description}</Text>
        </View>
      )
      text = (
        <View style={styles.textContainer}>
          <MarkdownText>{this.props.trafficFine.text}</MarkdownText>
        </View>
      )
    }

    return (
      <TouchableOpacity {...props}>
        {title}
        {text}
      </TouchableOpacity>
    )
  }
}
