import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import PhoneInput from 'react-native-phone-input'
import styles from './Styles/PhoneNumberFieldStyle'

export default class PhoneNumberField extends Component {
  // // Prop type warnings
  static propTypes = {
    value: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChangePhoneNumber: PropTypes.func.isRequired,
    onSubmitEditing: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    const error = this.props.error ? (<Text style={styles.errorText}>{this.props.error}</Text>) : null
    return (
      <View>
        <View style={styles.formField}>
          <PhoneInput
            ref={(phoneInput) => { this.phoneInput = phoneInput }}
            initialCountry='zw'
            flagStyle={styles.flagStyle}
            textStyle={styles.textInput}
            style={styles.phoneInput}
            value={this.props.value}
            placeholder={this.props.label}
            onChangePhoneNumber={this.props.onChangePhoneNumber}
            textProps={{
              placeholder: '+263718384668',
              enablesReturnKeyAutomatically: true,
              blurOnSubmit: true,
              onSubmitEditing: this.props.onSubmitEditing
            }}
          />
        </View>
        {error}
      </View>
    )
  }
}
