import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text } from 'react-native'
import styles from './Styles/ExpandableListItemStyle'
import { Colors } from '../Themes'
import MarkdownText from './MarkdownText'

export default class ExpandableListItem extends Component {
  static propTypes = {
    cardId: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    height: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.renderItems = this.renderItems.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.cardId)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardId)
  }

  handleItemPress (item) {

  }

  handleItemLongPress (item) {

  }

  renderItems () {
    const {items: trafficFines} = this.props.item
    if (trafficFines && typeof trafficFines === 'object') {
      return Object.keys(trafficFines).map((key, cardIndex) => {
        const trafficFine = trafficFines[key]
        const props = {
          style: [styles.container],
          shadowColor: Colors.charcoal,
          shadowOffset: {width: 4, height: 4},
          shadowOpacity: 0.5,
          shadowRadius: 4,
          onPress: this.handleItemPress,
          onLongPress: this.handleItemLongPress
        }
        return (
          <TouchableOpacity
            key={cardIndex}
            {...props}
          >
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{trafficFine.title}</Text>
            </View>
            <View style={styles.fineContainer}>
              <Text style={styles.fine}>{trafficFine.description}</Text>
            </View>
            <MarkdownText>{trafficFine.text}</MarkdownText>
          </TouchableOpacity>
        )
      })
    }
    return null
  }

  render () {
    const cardStyles = {}
    let hoverContent = null
    let selectedContent = null
    let title = (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{this.props.item.title}</Text>
      </View>
    )
    let description = this.props.item.description ? (
      <View style={styles.fineContainer}>
        <Text style={styles.fine}>{this.props.item.description}</Text>
      </View>
    ) : null
    let text = (<MarkdownText>{this.props.item.text}</MarkdownText>)
    let props = {
      ...this.props,
      style: [styles.container, this.props.style, cardStyles],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    if (this.props.selected === this.props.cardId) {
      props.disabled = true
      hoverContent = this.props.item.items && typeof this.props.item.items === 'object' ? (
        <View style={styles.hoverContent}>
          <Text>{Object.keys(this.props.item.items).length} fines</Text>
        </View>
      ) : null
      selectedContent = (
        <View style={styles.selectedContent}>
          <View style={styles.section}>
            <Text style={styles.title}>Fines </Text>
          </View>
          {this.renderItems()}
        </View>
      )
    }
    if (this.props.hovered === this.props.cardId) {
      hoverContent = this.props.item.items && typeof this.props.item.items === 'object' ? (
        <View style={styles.hoverContent}>
          <Text>{Object.keys(this.props.item.items).length} fines</Text>
        </View>
      ) : null
    }
    if ((this.props.selected !== this.props.cardId) && typeof this.props.selected === 'number') {
      cardStyles.height = 0
      cardStyles.overflow = 'hidden'
      hoverContent = null
      selectedContent = null
      title = null
      text = null
      props = {}
    }
    return (
      <View>
        <TouchableOpacity
          {...props}
        >
          {title}
          {description}
          {text}
          {hoverContent}
        </TouchableOpacity>
        {selectedContent}
      </View>
    )
  }
}
