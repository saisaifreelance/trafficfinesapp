import React from 'react'
import { DrawerNavigator } from 'react-navigation'
import DonutScreen from '../Containers/DonutScreen'
import HomeScreen from '../Containers/HomeScreen'
import DrawerContainer from '../Containers/DrawerContainer'

export default DrawerNavigator({
  Home: {
    screen: DonutScreen
  }
}, {
  contentComponent: DrawerContainer
})
