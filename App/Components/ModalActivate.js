import React from 'react'
import ModalActivateContainer from '../Containers/ModalActivateContainer'

export default (props) => (<ModalActivateContainer {...props} />)
