import React, { Component } from 'react'
import { View, TouchableWithoutFeedback, Keyboard } from 'react-native'

export default class KeyboardDismiss extends Component {
  render () {
    return (
      <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
        <View style={{}}>
          {this.props.children}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
