import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Picker, Modal, TouchableOpacity } from 'react-native'
import styles from './Styles/SelectFieldStyle'
import { Colors } from '../Themes'

export default class SelectField extends Component {
  // // Prop type warnings
  static propTypes = {
    value: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onValueChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    })),
    pickerProps: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.toggleModal = this.toggleModal.bind(this)
    this.state = {
      visible: false
    }
  }

  // // Defaults for props
  static defaultProps = {
    pickerProps: {}
  }

  toggleModal () {
    this.setState({visible: !this.state.visible})
  }

  render () {

    let style = {}
    let value = ''
    const error = this.props.error ? (<Text style={styles.errorText}>{this.props.error}</Text>) : null
    const options = this.props.options.map((option, index) => {
      if (this.props.value === option.value) {
        value = option.label
      }
      return (<Picker.Item key={index} label={option.label} value={option.value}/>)
    })

    if (!this.props.value) {
      value = this.props.label
      style.color = Colors.steel
    }

    return (
      <View>
        <TouchableOpacity style={styles.formField} onPress={this.toggleModal}>
          <Text style={[styles.textInput, style]}>{value}</Text>
        </TouchableOpacity>
        {error}
        <Modal
          animationType={'slide'}
          visible={this.state.visible}
          transparent
          onRequestClose={this.toggleModal}>
          <TouchableOpacity style={styles.modalContainer} onPress={this.toggleModal}>
            <View style={styles.pickerContainer}>
              <View style={styles.pickerHeaderContainer}><Text style={styles.pickerHeaderText}>Done</Text></View>
              <Picker
                selectedValue={this.props.value}
                onValueChange={this.props.onValueChange}>
                {options}
              </Picker>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
    )
  }
}
