import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import styles from './Styles/BadgeStyle'

export default class Badge extends Component {
  // Prop type warnings
  static propTypes = {
    style: PropTypes.object,
    children: PropTypes.string.isRequired,
  }

  // Defaults for props
  static defaultProps = {
    style: {}
  }

  render () {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.text}>{this.props.children}</Text>
      </View>
    )
  }
}
