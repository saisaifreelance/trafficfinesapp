import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import { NavigationActions } from 'react-navigation'
import IconButton from './IconButton'
import Button from './Button'
import styles from './Styles/ModalRateStyle'
import {Colors} from '../Themes'
import Communication from '../Services/Communication'
const communication = Communication.create()

const Stars = ({rating, onChangeRating}) => {
  const stars = []
  for (let i = 0; i < 5; i++) {
    const name = rating > i ? 'star' : 'star-border'
    stars.push(
      <IconButton
        onPress={onChangeRating.bind(undefined, i + 1)}
        key={i} backgroundColor={Colors.transparent}
        size={36}
        color={Colors.amber}
        icon={name}
      />
    )
  }

  return (
    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>{stars}</View>
  )
}

export default class ModalRate extends Component {
  // // Prop type warnings
  static propTypes = {
    close: PropTypes.func
  }

  static defaultProps = {}

  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.close = this.close.bind(this)
    this.state = {
      instructions: null,
      rating: 0
    }
  }

  onSubmit () {
    const {instructions, rating} = this.state
    const {doTag} = this.props

    if (rating >= 4) {
      return communication.rate()
    }

    if (!instructions && rating < 4) {
      return this.setState({instructions: true})
    }

    if (instructions) {
      this.props.navigation.dispatch({
        type: NavigationActions.NAVIGATE,
        routeName: 'ModalConnect',
        params: {
          title: 'Submit Feedback',
          description: 'Submit feedback to a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
          message: 'Hi, I\'m a Road Rules App user, and I have feedback for your app.',
          contact: {
            name: 'Road Rules App',
            phone: {
              office: {
                label: 'office',
                value: '+263718384668'
              }
            },
            whatsapp: {
              office: {
                label: 'office',
                value: '+263718384668'
              }
            },
            messenger: {
              office: {
                label: 'office',
                value: 'RoadRulesApp'
              }
            },
          }
        }
      })
      // this.props.openModal(<ModalConnect title={"Rate Us"} description={""} message={{
      //   text_out: doTag `Hi, I'm a Road Rules App user, reviewing your app`,
      //   text_in: doTag `Hi, Thanks for your interest in rating our app`,
      // }}/>)
    }
  }

  close () {
    if (this.state.instructions) {
      return this.setState({instructions: null})
    }
    if (this.props.close) {
      return this.props.close()
    }
    this.props.navigation.goBack()
  }

  render () {
    const {instructions, rating} = this.state
    let content = null
    if (!instructions) {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='close' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Please help us grow by leaving a (5) star rating for us and an excellent review on Google Playstore.</Text>
          </View>
          <View style={styles.section}>
            <Stars rating={rating} onChangeRating={(rating) => this.setState({rating})}/>
            <Button onPress={this.onSubmit.bind(this)}>RATE US</Button>
          </View>
        </View>
      )
    } else {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='arrow-back' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Please click OK if you would like to speak to a Road Rules representative before you review on Google Playstore.</Text>
          </View>
          <View style={styles.section}>
            <Button onPress={this.onSubmit.bind(this)}>OK</Button>
          </View>
        </View>
      )
    }
    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            {content}
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
