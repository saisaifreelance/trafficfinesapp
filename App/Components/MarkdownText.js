import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image } from 'react-native'
import Markdown from 'react-native-simple-markdown'
import styles, { markdownStyles } from './Styles/MarkdownTextStyle'

import { Metrics, Fonts, Colors } from '../Themes'

const {screenWidth: width, screenHeight: height} = Metrics
const IMAGE_WIDTH = width - 32
const IMAGE_HEIGHT = width - 32


const CustomImageComponent = (props) => {
    const {source} = props
    const uri = source.slice(0, source.indexOf('.'))
    return (<Image source={{uri}} style={{width: IMAGE_WIDTH, height: IMAGE_HEIGHT}} resizeMode={"contain"}/>
    )
}

export default class MarkdownText extends Component {
  // // Prop type warnings
  static propTypes = {
    children: PropTypes.string.isRequired,
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <Markdown style={styles.container} styles={markdownStyles} rules={rules}>{this.props.children}</Markdown>
    )
  }
}

const rules = {
    image: {
        react: (node, output, state) => (
            <CustomImageComponent
                key={state.key}
                source={node.target}
            />
        ),
    },
}
