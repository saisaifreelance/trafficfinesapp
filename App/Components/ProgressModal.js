import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator } from 'react-native'
import styles from './Styles/ProgressModalStyle'

export default class ProgressModal extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <ActivityIndicator animating size='large' color={'#fff'} />
          <Text style={styles.message}>{this.props.message}</Text>
        </View>
      </View>
    )
  }
}
