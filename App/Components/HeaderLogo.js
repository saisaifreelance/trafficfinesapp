import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image } from 'react-native'
import styles from './Styles/HeaderLogoStyle'
import { Images } from '../Themes'

export default class HeaderLogo extends Component {
  //  /Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.onLayout = this.onLayout.bind(this)
    this.state = {
      height: null,
      width: null
    }
  }

  onLayout ({ nativeEvent: { layout: { width, height, x, y } } }) {
    this.setState({ width, height })
  }

  render () {
    const {height, width} = this.state
    const style = {}
    let content = null
    if (height && width && height > 140) {
      style.height = height
      style.width = width - (2 * 20) - (2 * 64)
      content = (<View style={[styles.logoContainer]}>
        <View style={styles.logoFlank} />
        <Image
          source={Images.logo}
          resizeMode='contain'
          resizeMethod='scale'
          style={[styles.logoCenter, style]} />
        <View style={styles.logoFlank} />
      </View>)
    }
    return (
      <View style={styles.container} onLayout={this.onLayout}>
        {content}
      </View>
    )
  }
}
