import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/IconButtonStyle'
import { Colors } from '../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class IconButton extends Component {
  // Prop type warnings
  static propTypes = {
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    size: PropTypes.number.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
  }

  // Defaults for props
  static defaultProps = {
    style: {},
    size: 24,
    backgroundColor: Colors.charcoal,
    color: Colors.snow,
    icon: 'close',
    onPress: () => {},
  }

  render () {
    const props = {...this.props}
    props.dimens = props.size * 3 / 2
    const {size, backgroundColor, color, icon, dimens, onPress} = props
    const style = [{
      width: dimens,
      height: dimens,
      borderRadius: dimens / 2,
      backgroundColor,
      justifyContent: 'center',
      alignItems: 'center'
    }, props.style]
    if (icon === 'none') {
      return null
    }
    return (
      <TouchableOpacity
        onPress={onPress}
        style={style}>
        <Icon name={icon} size={size} color={color} />
      </TouchableOpacity>
    )
  }
}
