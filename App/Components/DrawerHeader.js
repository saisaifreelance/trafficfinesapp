import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import { CachedImage } from 'react-native-img-cache'
import styles from './Styles/DrawerHeaderStyle'
import Button from './Button'
import HeaderLogo from './HeaderLogo'
import Profile from '../Types/Profile'

export default class DrawerHeader extends Component {
  // // Prop type warnings
  static propTypes = {
    profile: Profile.isRequired,
    logout: PropTypes.func.isRequired,
    updateProfile: PropTypes.func.isRequired,
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    let profile = null
    let header = null
    if (this.props.profile && typeof this.props.profile === 'object') {
      profile = (
        <View style={styles.profileContainer}>
          <Text style={styles.profile}>{this.props.profile.name}</Text>
          <Text>{this.props.profile.phoneNumber}</Text>
        </View>
      )
      if (this.props.profile.profile_pic && typeof this.props.profile.profile_pic  === 'object') {
        header = (
          <View style={[styles.profilePicContainer]}>
            <CachedImage source={{uri: this.props.profile.profile_pic.downloadUrl}} style={[styles.profilePic]} />
          </View>
        )
      }
    }
    if (!header) {
      header = (
        <View style={[styles.logoContainer, {}]}>
          <HeaderLogo />
        </View>
      )
    }

    return (
      <View style={[styles.container, {}]}>
        {header}
        {profile}
        <View style={styles.buttons}>
          <Button
            onPress={this.props.logout}
            style={styles.button}
            textStyle={styles.buttonText}>logout</Button>
          <Button
            onPress={this.props.updateProfile}
            style={styles.button}
            textStyle={styles.buttonText}>update profile</Button>
        </View>
      </View>
    )
  }
}
