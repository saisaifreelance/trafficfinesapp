import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import IconButton from './IconButton'
import Button from './Button'
import styles from './Styles/ModalNotificationStyle'

export default class ModalNotification extends Component {
  // // Prop type warnings
  static propTypes = {
    notification: PropTypes.shape({
      title: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      prompt: PropTypes.string.isRequired,
      action: PropTypes.string.isRequired,
      payload: PropTypes.object
    }).isRequired,
    close: PropTypes.func.isRequired,
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.onPress = this.onPress.bind(this)
    this.close = this.close.bind(this)
  }

  onPress () {
    alert('on press')
  }

  close () {
    if (this.props.close) {
      return this.props.close()
    }
    this.props.navigation.goBack()
  }

  render () {
    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            <View style={styles.container}>
              <View style={styles.modalControlsContainer}>
                <IconButton icon='close' onPress={this.close}/>
              </View>
              <View style={styles.section}>
                <Text style={styles.title}>{this.props.notification.title}</Text>
                <Text style={styles.description}>{this.props.notification.message}</Text>
              </View>
              <View style={styles.section}>
                <Button onPress={this.onPress}>{this.props.notification.prompt}</Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
