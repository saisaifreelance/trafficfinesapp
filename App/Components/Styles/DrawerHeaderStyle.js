import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.logo,
  logoContainer: {
    height: Metrics.drawerLogoHeight
  },
  profilePicContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: Metrics.smallMargin,
  },
  profilePic: {
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  container: {
    ...ApplicationStyles.screen.container,
    backgroundColor: Colors.snow,
    paddingTop: Metrics.toolbarPaddingTop
  },
  profileContainer: {
    marginVertical: Metrics.smallMargin,
    alignItems: 'center'
  },
  profile: {
    ...Fonts.style.normal,
    marginVertical: Metrics.smallMargin,
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    margin: Metrics.smallMargin,
    flex: 1,
  },
  buttonText: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
    color: Colors.snow
  }
})
