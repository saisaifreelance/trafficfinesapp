import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin,
    flexDirection: 'row'
  },
  text: {
    ...Fonts.style.h5,
    flex: 1,
  },
  icon: {
    marginRight: Metrics.smallMargin
  }
})
