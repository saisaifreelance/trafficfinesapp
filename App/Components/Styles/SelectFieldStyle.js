import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  ...ApplicationStyles.form,
  modalContainer: {
    justifyContent: 'flex-end',
    backgroundColor: 'transparent',
    flex: 1,
  },
  pickerContainer: {
    backgroundColor: Colors.steel
  },
  pickerHeaderContainer: {
    padding: Metrics.baseMargin,
  },
  pickerHeaderText: {
    ...Fonts.style.h5,
  }
})
