import { StyleSheet } from 'react-native'
import { Metrics, Fonts, Colors, ApplicationStyles } from '../../Themes'

export default StyleSheet.create({
  container: {
    height: Metrics.parallaxHeaderHeight,
    width: Metrics.screenWidth
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: Metrics.parallaxHeaderHeight,
    width: Metrics.screenWidth
  },
  backgroundOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  title: {
    ...Fonts.style.h5,
    textAlign: 'center',
    color: Colors.snow
  },
  subTitle: {
    ...Fonts.style.h6,
    textAlign: 'center',
    color: Colors.snow
  }
})
