import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {

  },
  textStyle: {
    ...Fonts.style.h5,
  }
})
