import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginHorizontal: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin
  },
  divider: {
    marginVertical: Metrics.smallMargin,
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1
  },
  titleContainer: {
    flexDirection: 'row'
  },
  title: {
    ...Fonts.style.h5,
    flex: 1,
  },
  fineContainer: {
    margin: Metrics.baseMargin
  },
  fine: {
    ...Fonts.style.h5,
    color: Colors.bloodOrange,
    textAlign: 'right',
    marginLeft: Metrics.baseMargin
  },
  section: {
    margin: Metrics.smallMargin
  },
  hoverContent: {
    marginTop: Metrics.smallMargin
  },
  hoverContentText: {
    color: Colors.blue,
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
    textAlign: 'right'
  }
})
