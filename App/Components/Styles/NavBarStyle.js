import { StyleSheet } from 'react-native'
import { Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    height: Metrics.toolbarHeight,
    paddingTop: Metrics.toolbarPaddingTop,
  }
})
