import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginHorizontal: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin
  },
  titleContainer: {
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1
  },
  title: {
    ...Fonts.style.h5
  },
  fineContainer: {
    margin: Metrics.baseMargin
  },
  fine: {
    ...Fonts.style.h5,
    color: Colors.bloodOrange,
  },
  section: {
    margin: Metrics.smallMargin
  }
})
