import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/ChatPromptStyle'
import { Fonts, Colors } from '../Themes'

export default class ChatPrompt extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <Icon name='chat' size={Fonts.size.h2} color={Colors.charcoal} style={styles.icon} />
        <Text style={styles.text}>Click here to discuss this traffic fine with other users</Text>
      </TouchableOpacity>
    )
  }
}
