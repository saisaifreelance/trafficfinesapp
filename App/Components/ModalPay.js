import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import IconButton from './IconButton'
import Button from './Button'
import styles from './Styles/ModalPayStyle'
import { Colors, Metrics } from '../Themes'

export default class ModalPay extends Component {
  // // Prop type warnings
  static propTypes = {
    close: PropTypes.func
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.state = {
      instructions: null
    }
    this.onSelect = this.onSelect.bind(this)
    this.close = this.close.bind(this)
  }

  onSelect (instructions) {
    return this.setState({instructions})
  }

  close () {
    if (this.state.instructions) {
      return this.setState({instructions: null})
    }
    if (this.props.close) {
      return this.props.close()
    }
    if (this.props.navigation) {
      return this.props.navigation.goBack()
    }
  }

  render () {
    const {instructions} = this.state
    const ecocashShortCode = '*151*2*2*88782*2#'
    const ecocashMerchantCode = '88782'
    let content = null

    if (!instructions) {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='close' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Click on one of the buttons below to pay $2.00 to Road Rules via Ecocash, Telecash or OneWallet</Text>
          </View>
          <View style={styles.section}>
            <Button
              more
              color={Colors.snow}
              backgroundColor={Colors.ecocash}
              style={{marginTop: Metrics.baseMargin}}
              onPress={this.onSelect.bind(this, 'ecocash')}>Ecocash</Button>

            <Button
              more
              color={Colors.snow}
              backgroundColor={Colors.telecash}
              style={{marginTop: Metrics.baseMargin}}
              onPress={this.onSelect.bind(this, 'telecash')}>TeleCash</Button>

            <Button
              more
              color={Colors.snow}
              backgroundColor={Colors.onewallet}
              style={{marginTop: Metrics.baseMargin}}
              onPress={this.onSelect.bind(this, 'one_wallet')}>OneWallet</Button>
          </View>
        </View>
      )
    }

    if (instructions === 'telecash') {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='arrow-back' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Pay Via Telecash to Road Rules Merchant #211341</Text>
            <Text style={styles.description}>
              1. Dial *888#{'\n'}
              2. Select Option 5 'Make Payment'{'\n'}
              3. Select Option 3 'Pay Merchant'{'\n'}
              4. Enter Merchant Code 211341 and click 'Send'{'\n'}
              5. Enter Amount $2{'\n'}
              6. Enter your Telecash Pin{'\n'}
              7. Enter 1 to Confirm{'\n'}
            </Text>
          </View>
        </View>
      )
    }

    if (instructions === 'ecocash') {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='arrow-back' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Pay Via Ecocash to Road Rules Merchant #{ecocashMerchantCode}</Text>
            <Text style={styles.description}>
              1. Dial {ecocashShortCode}{'\n'}
              2. Enter ecocash your pin
            </Text>
          </View>
        </View>
      )
    }

    if (instructions === 'one_wallet') {
      content = (
        <View style={styles.container}>
          <View style={styles.modalControlsContainer}>
            <IconButton icon='arrow-back' onPress={this.close}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.title}>Pay Via One Wallet to Road Rules Merchant # 0718 384 668</Text>
            <Text style={styles.description}>
              1. From OneWallet select Send Cash.{'\n'}
              2. Enter recipient phone number (0718 384 668).{'\n'}
              3. Enter the amount ($2).{'\n'}
              4. Enter Secret Pin.{'\n'}
              5. Confirm transaction.
              6. You receive confirmation message.
            </Text>
          </View>
        </View>
      )
    }

    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            {content}
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
