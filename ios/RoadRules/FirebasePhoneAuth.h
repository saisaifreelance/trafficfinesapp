#ifndef FirebasePhoneAuth_h
#define FirebasePhoneAuth_h

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface FirebasePhoneAuth : RCTEventEmitter <RCTBridgeModule>
@end

#endif
