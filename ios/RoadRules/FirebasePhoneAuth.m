#import "FirebasePhoneAuth.h"
#import <FirebaseAuth/FirebaseAuth.h>

@implementation FirebasePhoneAuth

RCT_EXPORT_MODULE();

// PS: These events are for Android only. Its here just for convinience, to avoid crash on JS.
NSString *ON_VERIFICATION_COMPLETED = @"onFirebasePhoneVerificationCompleted";
NSString *ON_CODE_AUTO_RETRIEVAL_TIMEOUT = @"onFirebasePhoneCodeAutoRetrievalTimeOut";

- (NSDictionary *)constantsToExport
{
  return @{
           @"ON_VERIFICATION_COMPLETED": ON_VERIFICATION_COMPLETED,
           @"ON_CODE_AUTO_RETRIEVAL_TIMEOUT": ON_CODE_AUTO_RETRIEVAL_TIMEOUT,
           };
};

- (NSArray<NSString *> *)supportedEvents
{
  return @[ON_VERIFICATION_COMPLETED, ON_CODE_AUTO_RETRIEVAL_TIMEOUT];
}

RCT_EXPORT_METHOD(verifyPhoneNumber:(NSString *)userInput
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  [[FIRPhoneAuthProvider provider]
   verifyPhoneNumber:userInput
   completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
     if (error) {
       [self promiseRejectAuthException:reject error:error];
     } else {
       resolve(verificationID);
     }
   }];
}

RCT_EXPORT_METHOD(signInWithCredential:(NSString *)userInput verificationID:(NSString *)verificationID
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                   credentialWithVerificationID:verificationID
                                   verificationCode:userInput];
  [[FIRAuth auth]
   signInWithCredential:credential
   completion:^(FIRUser *user, NSError *error) {
     if (error) {
       [self promiseRejectAuthException:reject error:error];
     } else {
       [self promiseWithUser:resolve rejecter:reject user:user];
     }
   }];
}

RCT_EXPORT_METHOD(getCurrentUser:
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  if ([FIRAuth auth].currentUser) {
    [self promiseWithUser:resolve rejecter:reject user:[FIRAuth auth].currentUser];
  } else {
    resolve(NULL);
  }
}



/**
 Reject a promise with an auth exception
 
 @param reject RCTPromiseRejectBlock
 @param error NSError
 */
- (void) promiseRejectAuthException:(RCTPromiseRejectBlock) reject error:(NSError *)error {
  NSString *code = @"auth/unknown";
  NSString *message = [error localizedDescription];
  
  switch (error.code) {
    case FIRAuthErrorCodeInvalidCustomToken:
      code = @"auth/invalid-custom-token";
      message = @"The custom token format is incorrect. Please check the documentation.";
      break;
    case FIRAuthErrorCodeCustomTokenMismatch:
      code = @"auth/custom-token-mismatch";
      message = @"The custom token corresponds to a different audience.";
      break;
    case FIRAuthErrorCodeInvalidCredential:
      code = @"auth/invalid-credential";
      message = @"The supplied auth credential is malformed or has expired.";
      break;
    case FIRAuthErrorCodeInvalidEmail:
      code = @"auth/invalid-email";
      message = @"The email address is badly formatted.";
      break;
    case FIRAuthErrorCodeWrongPassword:
      code = @"auth/wrong-password";
      message = @"The password is invalid or the user does not have a password.";
      break;
    case FIRAuthErrorCodeUserMismatch:
      code = @"auth/user-mismatch";
      message = @"The supplied credentials do not correspond to the previously signed in user.";
      break;
    case FIRAuthErrorCodeRequiresRecentLogin:
      code = @"auth/requires-recent-login";
      message = @"This operation is sensitive and requires recent authentication. Log in again before retrying this request.";
      break;
    case FIRAuthErrorCodeAccountExistsWithDifferentCredential:
      code = @"auth/account-exists-with-different-credential";
      message = @"An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.";
      break;
    case FIRAuthErrorCodeEmailAlreadyInUse:
      code = @"auth/email-already-in-use";
      message = @"The email address is already in use by another account.";
      break;
    case FIRAuthErrorCodeCredentialAlreadyInUse:
      code = @"auth/credential-already-in-use";
      message = @"This credential is already associated with a different user account.";
      break;
    case FIRAuthErrorCodeUserDisabled:
      code = @"auth/user-disabled";
      message = @"The user account has been disabled by an administrator.";
      break;
    case FIRAuthErrorCodeUserTokenExpired:
      code = @"auth/user-token-expired";
      message = @"The user's credential is no longer valid. The user must sign in again.";
      break;
    case FIRAuthErrorCodeUserNotFound:
      code = @"auth/user-not-found";
      message = @"There is no user record corresponding to this identifier. The user may have been deleted.";
      break;
    case FIRAuthErrorCodeInvalidUserToken:
      code = @"auth/invalid-user-token";
      message = @"The user's credential is no longer valid. The user must sign in again.";
      break;
    case FIRAuthErrorCodeWeakPassword:
      code = @"auth/weak-password";
      message = @"The given password is invalid.";
      break;
    case FIRAuthErrorCodeOperationNotAllowed:
      code = @"auth/operation-not-allowed";
      message = @"This operation is not allowed. You must enable this service in the console.";
      break;
    case FIRAuthErrorCodeNetworkError:
      code = @"auth/network-error";
      message = @"A network error has occurred, please try again.";
      break;
    case FIRAuthErrorCodeInternalError:
      code = @"auth/internal-error";
      message = @"An internal error has occurred, please try again.";
      break;
      
      // unsure of the below codes so leaving them as the default error message
    case FIRAuthErrorCodeTooManyRequests:
      code = @"auth/too-many-requests";
      break;
    case FIRAuthErrorCodeProviderAlreadyLinked:
      code = @"auth/provider-already-linked";
      break;
    case FIRAuthErrorCodeNoSuchProvider:
      code = @"auth/no-such-provider";
      break;
    case FIRAuthErrorCodeInvalidAPIKey:
      code = @"auth/invalid-api-key";
      break;
    case FIRAuthErrorCodeAppNotAuthorized:
      code = @"auth/app-not-authorised";
      break;
    case FIRAuthErrorCodeExpiredActionCode:
      code = @"auth/expired-action-code";
      break;
    case FIRAuthErrorCodeInvalidMessagePayload:
      code = @"auth/invalid-message-payload";
      break;
    case FIRAuthErrorCodeInvalidSender:
      code = @"auth/invalid-sender";
      break;
    case FIRAuthErrorCodeInvalidRecipientEmail:
      code = @"auth/invalid-recipient-email";
      break;
    case FIRAuthErrorCodeKeychainError:
      code = @"auth/keychain-error";
      break;
      
      // phone auth codes
    case FIRAuthErrorCodeMissingPhoneNumber:
      code = @"auth/missing-phone-number";
      message = @"Please provide a phone number.";
    case FIRAuthErrorCodeInvalidPhoneNumber:
      code = @"auth/invalid-phone-number";
      message = @"Please enter a valid phone number in the correct international format, e.g. +263718384668";
      break;
    case FIRAuthErrorCodeMissingVerificationCode:
      code = @"auth/missing-verification-code";
      message = @"Please enter the verification code that was sent to you via SMS.";
      break;
    case FIRAuthErrorCodeInvalidVerificationCode:
      code = @"auth/invalide-verification-code";
      message = @"You have entered the wrong verification code, please re-check the SMS sent to you and enter the correct code & press Verify. If the problem continues get a new code by pressing Resend Code.";
      break;
    case FIRAuthErrorCodeMissingVerificationID:
      code = @"auth/missing-verification-id";
      message = @"Please provide a verification ID.";
      break;
    case FIRAuthErrorCodeInvalidVerificationID:
      code = @"auth/invalid-verification-id";
      message = @"Please provide a valid verification ID.";
      break;
    case  FIRAuthErrorCodeMissingAppCredential:
      code = @"auth/missing-app-credential";
      message = @"Please provide an app credential.";
      break;
    case  FIRAuthErrorCodeInvalidAppCredential:
      code = @"auth/invalid-app-credential";
      message = @"Please provide a valid app credential.";
      break;
    case  FIRAuthErrorCodeSessionExpired:
      code = @"auth/session-expired";
      message = @"SMS code has expired. Please click Resend Code to get a new verification code.";
      break;
    case  FIRAuthErrorCodeQuotaExceeded:
      code = @"auth/quota-exceeded";
      message = @"SMS quota has been exceeded, please contact admin.";
      break;
    default:
      break;
  }
  reject(code, message, error);
}

/**
 Resolve or reject a promise based on FIRUser value existance
 
 @param resolve RCTPromiseResolveBlock
 @param reject RCTPromiseRejectBlock
 @param user FIRUser
 */
- (void) promiseWithUser:(RCTPromiseResolveBlock) resolve rejecter:(RCTPromiseRejectBlock) reject user:(FIRUser *) user {
  if (user) {
    NSDictionary *userDict = [self firebaseUserToDict:user];
    resolve(userDict);
  } else {
    [self promiseNoUser:resolve rejecter:reject isError:YES];
  }
  
}

/**
 Converts an array of FIRUserInfo instances into the correct format to match the web sdk
 
 @param providerData FIRUser.providerData
 @return NSArray
 */
- (NSArray <NSObject *> *) convertProviderData:(NSArray <id<FIRUserInfo>> *) providerData {
  NSMutableArray *output = [NSMutableArray array];
  
  for (id<FIRUserInfo> userInfo in providerData) {
    NSMutableDictionary *pData = [NSMutableDictionary dictionary];
    
    if (userInfo.providerID != nil) {
      [pData setValue: userInfo.providerID forKey:@"providerId"];
    }
    
    if (userInfo.uid != nil) {
      [pData setValue: userInfo.uid forKey:@"uid"];
    }
    
    if (userInfo.displayName != nil) {
      [pData setValue: userInfo.displayName forKey:@"displayName"];
    }
    
    if (userInfo.phoneNumber != nil) {
      [pData setValue: userInfo.phoneNumber forKey:@"phoneNumber"];
    }
    
    if (userInfo.photoURL != nil) {
      [pData setValue: [userInfo.photoURL absoluteString] forKey:@"photoURL"];
    }
    
    if (userInfo.email != nil) {
      [pData setValue: userInfo.email forKey:@"email"];
    }
    
    [output addObject:pData];
  }
  
  return output;
}

/**
 Converts a FIRUser instance into a dictionary to send via RNBridge
 
 @param user FIRUser
 @return NSDictionary
 */
- (NSDictionary *) firebaseUserToDict:(FIRUser *) user {
  NSMutableDictionary *userDict = [
                                   @{ @"uid": user.uid,
                                      @"email": user.email ? user.email : [NSNull null],
                                      @"emailVerified": @(user.emailVerified),
                                      @"isAnonymous": @(user.anonymous),
                                      @"phoneNumber": user.phoneNumber ? user.phoneNumber : [NSNull null],
                                      @"displayName": user.displayName ? user.displayName : [NSNull null],
                                      @"refreshToken": user.refreshToken,
                                      @"providerId": [user.providerID lowercaseString],
                                      @"providerData": [self convertProviderData: user.providerData]
                                      } mutableCopy
                                   ];
  
  if ([user valueForKey:@"photoURL"] != nil) {
    [userDict setValue: [user.photoURL absoluteString] forKey:@"photoURL"];
  }
  
  if ([user valueForKey:@"phoneNumber"] != nil) {
    [userDict setValue: user.phoneNumber ? user.phoneNumber : [NSNull null] forKey:@"phoneNumber"];
  }
  
  return userDict;
}

/**
 Resolve or reject a promise based on isError value
 
 @param resolve RCTPromiseResolveBlock
 @param reject RCTPromiseRejectBlock
 @param isError BOOL
 */
- (void) promiseNoUser:(RCTPromiseResolveBlock) resolve rejecter:(RCTPromiseRejectBlock) reject isError:(BOOL) isError {
  if (isError) {
    reject(@"auth/no-current-user", @"No user currently signed in.", nil);
  } else {
    resolve([NSNull null]);
  }
}

@end
