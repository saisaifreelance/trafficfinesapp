package co.neolab.trafficfines;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableArray;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.appinvite.FirebaseAppInvite;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class FirebaseInvitesModule extends ReactContextBaseJavaModule {
    public static final int RC_APP_INVITES_IN = 9002;
    private static final int RESULT_OK = -1;
    private GoogleApiClient mGoogleApiClient;

    private static ReactApplicationContext _context;

    public FirebaseInvitesModule(ReactApplicationContext _reactContext) {
        super(_reactContext);
        _context = _reactContext;
    }

    @Override
    public String getName() {
        return "FirebaseInvites";
    }

    public static void invitationsSentSuccessfully(String[] ids) {
        WritableMap params = Arguments.createMap();
        WritableArray paramsIds = Arguments.fromArray(ids);
        params.putArray("ids", paramsIds);
        sendEvent(_context, "firebaseInviteIds", params);
    }

    public static void invitationsFailedOrCanceled() {
        WritableMap params = Arguments.createMap();
        params.putString("message", "Sending failed or it was canceled");
        sendEvent(_context, "firebaseInvitesError", params);
    }

    @ReactMethod
    public void init() {

    }

    @ReactMethod
    public void invite(String message, String title, String deepLink) {
        Activity _activity = getCurrentActivity();
        _activity.runOnUiThread(new Runnable() {
            private String _message;
            private String _title;
            private String _deepLink;
            private Activity _activity;

            public Runnable init(String message, String title, String deepLink, Activity activity) {
                _message = message;
                _title = title;
                _deepLink = deepLink;
                _activity = activity;
                return (this);
            }

            @Override
            public void run() {
                Intent intent = new AppInviteInvitation.IntentBuilder(_title)
                        .setMessage(_message)
                        .setDeepLink(Uri.parse(_deepLink))
//                        .setCustomImage(Uri.parse(_image_url))
//                        .setCallToActionText(_cta_text)
                        .build();
                _activity.startActivityForResult(intent, RC_APP_INVITES_IN);
            }
        }.init(message, title, deepLink, _activity));
    }

    public static void onActivityResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
            invitationsSentSuccessfully(ids);
        } else {
            // Sending failed or it was canceled, show failure message to the user
            invitationsFailedOrCanceled();
        }
    }

    private static void sendEvent(ReactContext reactContext,
                                  String eventName,
                                  @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

}
